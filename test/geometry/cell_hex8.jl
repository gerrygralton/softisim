using SofTiSim

# Volume

@test volume(reference_element(Hex8)) ≈ 8

points = [Point(0, 0, 0), Point(1, 0, 0), Point(1, 3, 0), Point(0, 3, 0),
          Point(0, 0, 1), Point(1, 0, 2), Point(1, 3, 2), Point(0, 3, 1)];

@test volume(Hex8(1:8, points)) ≈ 4.5
