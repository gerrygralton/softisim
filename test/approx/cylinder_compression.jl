thismodule = @__MODULE__
if !isinteractive()
    outdir = joinpath(ENV["SOFTISIM_TEST_OUTPUT_DIR"], jobname)
    printstyled("Module name:        $thismodule\n", bold=true, color=:magenta)
    printstyled("Job name:           $jobname\n", bold=true, color=:magenta)
    printstyled("Output directory:   $outdir\n", bold=true, color=:magenta)
    printstyled("Material:           $material\n", bold=true, color=:magenta)
end
# Output directory to use when debugging script
if false
    outdir = abspath("./$jobname")
end

import CSV
using DataFrames: DataFrame
using LinearAlgebra
using Statistics

# Local parameters
height = 0.1
max_disp = -0.02  # compress by 20%

model[:scale_mass] = false

model[:progress_num_steps] = 500

solver = DynamicRelaxationParameters() # FIXME: see below (time_load must be 1.0)

model[:sections] = (
    all = (
        region = :all,
        density = 1000,
        material = material
    ),
)

model[:bcs] = (
    bot_fixed = DirichletBC(
        nodes = mesh.nsets["zmin"],
        dofs = 1:3,
    ),
    top_fixed = DirichletBC(
        nodes = mesh.nsets["zmax"],
        dofs = 1:2,
    ),
    top_displaced = DirichletBC(
        nodes = mesh.nsets["zmax"],
        dofs = 3:3,
        magnitude = max_disp, # FIXME: time_load must be 1 for this to work
        amplitude = smooth345,
    ),
)

model[:output_ppoints] = true
model[:output_qinfluence] = [1, 2, 3]
model[:output_qmesh] = true
model[:output_qpoints] = true

model[:output_history] = OutputHistory(
    user_functions = (
        # Average vertical displacement of displaced surface nodes
        disp_displaced       = G -> Statistics.mean(G.disp[mesh.nsets["zmax"], 3]),
        # Reaction forces
        forc_tot_z_displaced = G -> sum(G.forc_tot[mesh.nsets["zmax"], 3]),
        forc_ebc_z_displaced = G -> sum(G.forc_ebc[mesh.nsets["zmax"], 3]),
        forc_tot_z_fixed     = G -> sum(G.forc_tot[mesh.nsets["zmin"], 3]),
        forc_ebc_z_fixed     = G -> sum(G.forc_ebc[mesh.nsets["zmin"], 3]),
    ),
);

model[:output_field] = OutputFieldVtk(
    interval = (haskey(ENV, "SOFTISIM_TEST_OUTPUT_FIELD_INTERVAL") ?
                parse(Int, ENV["SOFTISIM_TEST_OUTPUT_FIELD_INTERVAL"]) :
                typemax(Int))
);

# Load benchmark field dataframe from CSV file
if haskey(data, :benchmark)
    benchmark_field = DataFrame(CSV.File(data.benchmark, comment="#"))
    rename_abaqus_to_softisim!(benchmark_field)
else
    benchmark_field = nothing
end

glob, output_filename, output_history_dataframe, results = solve(
    model,
    mesh,
    solver,
    outdir = outdir,
    benchmark_field = benchmark_field,
    overwrite = ENV["SOFTISIM_TEST_OVERWRITE_OUTPUT_DIR"] == "1" ? true : false);

exit_status = results.success ? "OK" : "ERROR"

# ------------------------------------------------------------------------------
# Verification

printstyled("Job name:           $jobname\n", bold=true, color=:magenta)
printstyled("max_disp:           $max_disp\n", bold=true, color=:magenta)
printstyled("Exit status:        $exit_status\n", bold=true, color=:magenta)

# uz at z=h/2 (zmid)
u = glob.disp[mesh.nsets["zmid"],3];
ue = 0.5*max_disp*ones(length(u));
uz_zmid_err  = norm(u - ue);
uz_zmid_mean = Statistics.mean(u)
uz_zmid_std  = Statistics.std(u)
printstyled("uz at z=h/2 plane  mean: $uz_zmid_mean  std: $uz_zmid_std  err: $uz_zmid_err",
            "\n", bold=true, color=:magenta)

# ur around a ring at z=h/2 (zmid_ring)
ux = glob.disp[mesh.nsets["zmid_ring"],1];
uy = glob.disp[mesh.nsets["zmid_ring"],2];
u = sqrt.(ux.^2 + uy.^2);
ur_zmid_ring_mean = Statistics.mean(u)
ur_zmid_ring_std  = Statistics.std(u)
printstyled("ur at z=h/2 ring   mean: $ur_zmid_ring_mean  std: $ur_zmid_ring_std",
            "\n", bold=true, color=:magenta)

# TODO: compare displacements node by node with benchmark solution
# TODO: compare forces node by node with benchmark solution
# Put the benchmark solution in _data
# Compare benchmark C3D4 with SofTiSim Tet4 P1 etc.
# For meshless the results might be more accurate than benchmarks
# but should be similar.
# TODO: compare total reaction forces with benchmark solution

# Differences between tolerance and result
diff_uz_zmid_err      = data.uz_zmid_err.tol      - uz_zmid_err
diff_uz_zmid_std      = data.uz_zmid_std.tol      - uz_zmid_std
diff_ur_zmid_ring_std = data.ur_zmid_ring_std.tol - ur_zmid_ring_std

# Save the results
if !isinteractive()
    open(ENV["SOFTISIM_TEST_RESULTS_FILENAME"], "a") do io
        print(io, "\n\n")
        println(io, "jobname                       $jobname")

        # Input data
        print(io, "\n")
        println(io, "material                      $material")

        # Success/Failure
        print(io, "\n")
        println(io, "Status:                       $exit_status")

        # Results
        print(io, "\n")
        println(io, "max_disp                      $max_disp")
        println(io, "maximum(glob.disp)            $(maximum(glob.disp))")
        println(io, "minimum(glob.disp)            $(minimum(glob.disp))")
        println(io, "maximum(glob.disp[:,1])       $(maximum(glob.disp[:,1]))")
        println(io, "minimum(glob.disp[:,1])       $(minimum(glob.disp[:,1]))")
        println(io, "maximum(glob.disp[:,2])       $(maximum(glob.disp[:,2]))")
        println(io, "minimum(glob.disp[:,2])       $(minimum(glob.disp[:,2]))")
        println(io, "maximum(glob.disp[:,3])       $(maximum(glob.disp[:,3]))")
        println(io, "minimum(glob.disp[:,3])       $(minimum(glob.disp[:,3]))")
        println(io, "uz_zmid_err                   $uz_zmid_err",       " (tol: ", data.uz_zmid_err.tol,      ", diff: ", diff_uz_zmid_err, ")")
        println(io, "uz_zmid_mean                  $uz_zmid_mean")
        println(io, "uz_zmid_std                   $uz_zmid_std",       " (tol: ", data.uz_zmid_std.tol,      ", diff: ", diff_uz_zmid_std, ")")
        println(io, "ur_zmid_ring_mean             $ur_zmid_ring_mean")
        println(io, "ur_zmid_ring_std              $ur_zmid_ring_std",  " (tol: ", data.ur_zmid_ring_std.tol, ", diff: ", diff_ur_zmid_ring_std, ")")
    end
end

# Check if tolerances should be updated
macro warn_update_tol(err, tol)
    quote
        if $err < 0.9 * $tol
            @warn string("Tolerance ", $(string(tol)), " should be updated\n",
                         "err: ", $err, "\n",
                         "tol: ", $tol)
        end
    end
end

@warn_update_tol uz_zmid_err      data.uz_zmid_err.tol
@warn_update_tol uz_zmid_std      data.uz_zmid_std.tol
@warn_update_tol ur_zmid_ring_std data.ur_zmid_ring_std.tol
