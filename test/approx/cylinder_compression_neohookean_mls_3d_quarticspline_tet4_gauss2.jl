if !isinteractive()
    jobname = splitext(basename(@__FILE__))[1]
end

using SofTiSim

data = (
    uz_zmid_err      = (tol = 1.88e-4, ),
    uz_zmid_std      = (tol = 2.44e-5, ),
    ur_zmid_ring_std = (tol = 4.60e-6, ),
)

material = NeoHookean(elastic_moduli(E=3000, nu=0.49))

mesh = readmesh("./_data/mesh/cylinder100mm_z6_r4_C3D4_e1824_n399_rev1.inp")

model = Dict()

model[:basic_function] = BasicQuarticSpline()
model[:use_exact_SF_derivatives] = true
model[:support_radius_dilatation] = 1.6
model[:use_variable_support_radius] = true
model[:use_base_functions] = 2
model[:use_EBCIEM] = true
model[:use_Simplified_EBCIEM] = true

model[:use_adaptive_integration] = false
model[:quadrature_degree] = 2

include("cylinder_compression.jl")

# ------------------------------------------------------------------------------
# Tests

@test results.success == true

# Mass
# exact volume is 0.0007853981633974484 but we don't use isoparametric mesh
@test isapprox(sum(glob.mass), 7.654e-04 * model[:sections][:all][:density], atol=0.0001)

# Prescribed displacement
# glob.disp should be almost exact for both finite element and meshfree methods
@test all(isapprox.(glob.disp[mesh.nsets["zmax"],3], max_disp))
@test all(isapprox.(glob.disp[mesh.nsets["zmax"],1:2], 0, atol=eps(Float64)))
@test all(isapprox.(glob.disp[mesh.nsets["zmin"],1:3], 0, atol=eps(Float64)))
# glob.dnod is not exact for meshfree because meshfree is not interpolating
# so use loose tolerance here
@test all(isapprox.(glob.dnod_curr[mesh.nsets["zmax"],3], max_disp, atol=1e-3))
@test all(isapprox.(glob.dnod_curr[mesh.nsets["zmax"],1:2], 0, atol=1e-3))
@test all(isapprox.(glob.dnod_curr[mesh.nsets["zmin"],1:3], 0, atol=1e-3))

# Predicted displacement
@test uz_zmid_err <= data.uz_zmid_err.tol
@test uz_zmid_std <= data.uz_zmid_std.tol
@test ur_zmid_ring_std <= data.ur_zmid_ring_std.tol

# TODO: Predicted reaction forces
