thismodule = @__MODULE__
if !isinteractive()
    outdir = joinpath(ENV["SOFTISIM_TEST_OUTPUT_DIR"], jobname)
    printstyled("Module name:        $thismodule\n", bold=true, color=:magenta)
    printstyled("Job name:           $jobname\n", bold=true, color=:magenta)
    printstyled("Output directory:   $outdir\n", bold=true, color=:magenta)
    printstyled("Material:           $material\n", bold=true, color=:magenta)
end
# Output directory to use when debugging script
if false
    outdir = abspath("./$jobname")
end

# Local parameters
height = 0.1
nx = 6
if isdefined(thismodule, :max_disp)
    @warn "Non-default max_disp used for this test: $max_disp"
else
    max_disp = -0.08  # compress by 80%
end

mesh = meshbox(Point(0,0,0), Point(height, height, height), nx, nx, nx, element)

model[:scale_mass] = true

model[:progress_num_steps] = 500

# FIXME: see below (time_load must be 1.0)
if isdefined(thismodule, :solver)
    @warn "Non-default solver used for this test: $solver"
else
    solver = DynamicRelaxationParameters(
        # FIXME: use default solver parameters (will need to update tolerances)
        time_increment_safety_factor = 3
    )
end

model[:bcs] = (
    dx_0 = DirichletBC(
        nodes = mesh.nsets["xmin"],
        dofs = 1:1,
    ),
    dy_0 = DirichletBC(
        nodes = mesh.nsets["ymin"],
        dofs = 2:2,
    ),
    dz_0 = DirichletBC(
        nodes = mesh.nsets["zmin"],
        dofs = 3:3,
    ),
    disp = DirichletBC(
        nodes = mesh.nsets["zmax"],
        dofs = 3:3,
        magnitude = max_disp, # FIXME: time_load must be 1 for this to work
        amplitude = smooth345,
    ),
)

model[:sections] = (
    all = (
        region = :all,
        density = 1000,
        material = material,
    ),
)

model[:output_field] = OutputFieldVtk(
    interval = (haskey(ENV, "SOFTISIM_TEST_OUTPUT_FIELD_INTERVAL") ?
                parse(Int, ENV["SOFTISIM_TEST_OUTPUT_FIELD_INTERVAL"]) :
                typemax(Int))
);

if !isdefined(thismodule, :qmesh)
    qmesh = nothing
end

glob, output_filename, output_history_dataframe, results = solve(
    model,
    mesh,
    solver,
    qmesh = qmesh,
    outdir = outdir,
    overwrite = ENV["SOFTISIM_TEST_OVERWRITE_OUTPUT_DIR"] == "1" ? true : false);

exit_status = results.success ? "OK" : "ERROR"

# ------------------------------------------------------------------------------
# Verification

printstyled("Job name:           $jobname\n", bold=true, color=:magenta)
printstyled("max_disp:           $max_disp\n", bold=true, color=:magenta)
printstyled("Exit status:        $exit_status\n", bold=true, color=:magenta)

# If max_disp is to large this may fail but we need uexact to compare in tests
# that are meant to fail (extreme deformations).
try
    global uexact = exact_cube_uniaxial(glob.coord,
                                 material,
                                 (height + max_disp)/height)
catch
    global uexact = Inf * ones(size(glob.disp))
end

# Print the NRMSE and test
# If the difference is positive the result is more accurate than tolerance
# and the tolerance should be reduced.

err_all = nrmse(glob.disp, uexact)
err_x   = nrmse(glob.disp[:,1], uexact[:,1])
err_y   = nrmse(glob.disp[:,2], uexact[:,2])
err_z   = nrmse(glob.disp[:,3], uexact[:,3])

# Differences between tolerance and result
diff_all = data.all.tol - err_all
diff_x   = data.x.tol   - err_x
diff_y   = data.y.tol   - err_y
diff_z   = data.z.tol   - err_z

# Print results
# printstyled("NRMSE glob.disp:    ", err_all, " (diff: ", diff_all, ")\n", bold=true, color=:magenta)
# printstyled("NRMSE glob.disp x:  ", err_x,   " (diff: ", diff_x, ")\n", bold=true, color=:magenta)
# printstyled("NRMSE glob.disp y:  ", err_y,   " (diff: ", diff_y, ")\n", bold=true, color=:magenta)
# printstyled("NRMSE glob.disp z:  ", err_z,   " (diff: ", diff_z, ")\n", bold=true, color=:magenta)
printstyled("NRMSE glob.disp:    ", err_all, " (diff: ", diff_all, ")\n", bold=true, color=:magenta)
printstyled("NRMSE glob.disp x:  ", err_x,   " (diff: ", diff_x, ")\n", bold=true, color=:magenta)
printstyled("NRMSE glob.disp y:  ", err_y,   " (diff: ", diff_y, ")\n", bold=true, color=:magenta)
printstyled("NRMSE glob.disp z:  ", err_z,   " (diff: ", diff_z, ")\n", bold=true, color=:magenta)

# Save the results
if !isinteractive()
    open(ENV["SOFTISIM_TEST_RESULTS_FILENAME"], "a") do io
        print(io, "\n\n")
        println(io, "jobname                       $jobname")

        # Input data
        print(io, "\n")
        println(io, "material                      $material")

        # Success/Failure
        print(io, "\n")
        println(io, "Status:                       $exit_status")

        # Results
        print(io, "\n")
        println(io, "max_disp                      $max_disp")
        println(io, "maximum(glob.disp)            $(maximum(glob.disp)) (exact = $(maximum(uexact)))")
        println(io, "minimum(glob.disp)            $(minimum(glob.disp)) (exact = $(minimum(uexact)))")
        println(io, "maximum(glob.disp[:,1])       $(maximum(glob.disp[:,1])) (exact = $(maximum(uexact[:,1])))")
        println(io, "minimum(glob.disp[:,1])       $(minimum(glob.disp[:,1])) (exact = $(minimum(uexact[:,1])))")
        println(io, "maximum(glob.disp[:,2])       $(maximum(glob.disp[:,2])) (exact = $(maximum(uexact[:,2])))")
        println(io, "minimum(glob.disp[:,2])       $(minimum(glob.disp[:,2])) (exact = $(minimum(uexact[:,2])))")
        println(io, "maximum(glob.disp[:,3])       $(maximum(glob.disp[:,3])) (exact = $(maximum(uexact[:,3])))")
        println(io, "minimum(glob.disp[:,3])       $(minimum(glob.disp[:,3])) (exact = $(minimum(uexact[:,3])))")
        println(io, "err_all                       $err_all (tol: $(data.all.tol)   diff: $(diff_all))")
        println(io, "err_x                         $err_x   (tol: $(data.x.tol)     diff: $(diff_x))")
        println(io, "err_y                         $err_y   (tol: $(data.y.tol)     diff: $(diff_y))")
        println(io, "err_z                         $err_z   (tol: $(data.z.tol)     diff: $(diff_z))")
    end
end

# Check if tolerances should be updated
macro warn_update_tol(err, tol)
    quote
        if $err < 0.9 * $tol
            @warn string("Tolerance ", $(string(tol)), " should be updated\n",
                         "err: ", $err, "\n",
                         "tol: ", $tol)
        end
    end
end

@warn_update_tol err_all data.all.tol
@warn_update_tol err_x   data.x.tol
@warn_update_tol err_y   data.y.tol
@warn_update_tol err_z   data.z.tol
