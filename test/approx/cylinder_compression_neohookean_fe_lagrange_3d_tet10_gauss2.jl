if !isinteractive()
    jobname = splitext(basename(@__FILE__))[1]
end

using SofTiSim
using DataFrames: DataFrame
import CSV

data = (
    uz_zmid_err      = (tol = 1.27e-3, ),
    uz_zmid_std      = (tol = 3.38e-5, ),
    ur_zmid_ring_std = (tol = 3.53e-5, ),

    benchmark        = "./_data/benchmarks/cylinder100mm_z6_r4_C3D10_e1824_n2717_rev1_C3D10_compression__field_i2_t0.2000.csv",
    bench_ux         = (tol = 3.13e-6, ),
    bench_uy         = (tol = 3.13e-6, ),
    bench_uz         = (tol = 3.04e-6, ),
    bench_fx         = (tol = 8.66e-7, ),
    bench_fy         = (tol = 8.81e-7, ),
    bench_fz         = (tol = 1.02e-6, ),
)

material = NeoHookean(elastic_moduli(E=3000, nu=0.49))

mesh = readmesh("./_data/mesh/cylinder100mm_z6_r4_C3D10_e1824_n2717_rev1.inp")

model = Dict()

model[:approx] = FELagrange(2)
model[:quadrature_degree] = 2

include("cylinder_compression.jl")

# ------------------------------------------------------------------------------
# Tests

@test results.success == true

# Mass
# exact volume is 0.0007853981633974484 but we don't use isoparametric mesh
@test isapprox(sum(glob.mass), 7.654e-04 * model[:sections][:all][:density], atol=0.0001)

# Prescribed displacement
# glob.disp should be almost exact for both finite element and meshfree methods
@test all(isapprox.(glob.disp[mesh.nsets["zmax"],3], max_disp))
@test all(isapprox.(glob.disp[mesh.nsets["zmax"],1:2], 0, atol=eps(Float64)))
@test all(isapprox.(glob.disp[mesh.nsets["zmin"],1:3], 0, atol=eps(Float64)))
# glob.dnod is not exact for meshfree because meshfree is not interpolating
# so use loose tolerance here
@test all(isapprox.(glob.dnod_curr[mesh.nsets["zmax"],3], max_disp, atol=1e-3))
@test all(isapprox.(glob.dnod_curr[mesh.nsets["zmax"],1:2], 0, atol=1e-3))
@test all(isapprox.(glob.dnod_curr[mesh.nsets["zmin"],1:3], 0, atol=1e-3))

# Predicted displacement
@test uz_zmid_err <= data.uz_zmid_err.tol
@test uz_zmid_std <= data.uz_zmid_std.tol
@test ur_zmid_ring_std <= data.ur_zmid_ring_std.tol

# Compare node by node with benchmark solution
a = DataFrame(CSV.File(joinpath(outdir, jobname*".field.csv")));
b = DataFrame(CSV.File(data.benchmark, comment="#"));

# Check field output
@test all(a.disp_1 .≈ glob.disp[:,1])
@test all(a.disp_2 .≈ glob.disp[:,2])
@test all(a.disp_3 .≈ glob.disp[:,3])
@test all(a.forc_tot_1 .≈ glob.forc_tot[:,1])
@test all(a.forc_tot_2 .≈ glob.forc_tot[:,2])
@test all(a.forc_tot_3 .≈ glob.forc_tot[:,3])

# Compare displacement node by node with benchmarks
@test nrmse(a.disp_1, b.U1) <= data.bench_ux.tol
@test nrmse(a.disp_2, b.U2) <= data.bench_uy.tol
@test nrmse(a.disp_3, b.U3) <= data.bench_uz.tol
@test_broken all(isapprox.(a.disp_1, b.U1, atol=1e-7))
@test_broken all(isapprox.(a.disp_2, b.U2, atol=1e-7))
@test_broken all(isapprox.(a.disp_3, b.U3, atol=1e-7))

# Compare reaction force node by node with benchmarks
@test nrmse(a.forc_tot_1, b.RF1) <= data.bench_fx.tol
@test nrmse(a.forc_tot_2, b.RF2) <= data.bench_fy.tol
@test nrmse(a.forc_tot_3, b.RF3) <= data.bench_fz.tol
@test all(isapprox.(a.forc_tot_1, b.RF1, atol=1e-5))
@test all(isapprox.(a.forc_tot_2, b.RF2, atol=1e-5))
@test all(isapprox.(a.forc_tot_3, b.RF3, atol=1e-5))
