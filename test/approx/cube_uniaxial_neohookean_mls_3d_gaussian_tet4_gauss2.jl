if !isinteractive()
    jobname = splitext(basename(@__FILE__))[1]
end

using SofTiSim

material = NeoHookean(elastic_moduli(E=3000, nu=0.49))

element = Tet4

data = (
    all = (tol = 1.53e-4, ),
    x   = (tol = 3.08e-4, ),
    y   = (tol = 3.08e-4, ),
    z   = (tol = 1.09e-4, ),
)

model = Dict()

model[:basic_function] = BasicGaussian()
model[:use_exact_SF_derivatives] = true
model[:support_radius_dilatation] = 1.6
model[:use_variable_support_radius] = true
model[:use_base_functions] = 2
model[:use_EBCIEM] = true
model[:use_Simplified_EBCIEM] = true

model[:use_adaptive_integration] = false
model[:quadrature_degree] = 2

include("cube_uniaxial.jl")

# ------------------------------------------------------------------------------
# Tests

@test results.success == true

# Prescribed displacement
# glob.disp should be almost exact for both finite element and meshfree methods
@test all(isapprox.(glob.disp[mesh.nsets["zmax"],3], max_disp))
@test all(isapprox.(glob.disp[mesh.nsets["xmin"],1], 0, atol=eps(Float64)))
@test all(isapprox.(glob.disp[mesh.nsets["ymin"],2], 0, atol=eps(Float64)))
@test all(isapprox.(glob.disp[mesh.nsets["zmin"],3], 0, atol=eps(Float64)))
# glob.dnod is not exact for meshfree because meshfree is not interpolating
# so use loose tolerance here
@test all(isapprox.(glob.dnod_curr[mesh.nsets["zmax"],3], max_disp, atol=1e-4))
@test all(isapprox.(glob.dnod_curr[mesh.nsets["xmin"],1], 0, atol=1e-4))
@test all(isapprox.(glob.dnod_curr[mesh.nsets["ymin"],2], 0, atol=1e-4))
@test all(isapprox.(glob.dnod_curr[mesh.nsets["zmin"],3], 0, atol=1e-4))

# Predicted displacement
@test err_all <= data.all.tol
@test err_x   <= data.x.tol
@test err_y   <= data.y.tol
@test err_z   <= data.z.tol

# TODO: Predicted reaction forces
