using Test
using SofTiSim

elemtypes = [Hex8, Tet4]

@testset "Test meshbox with different element types" for elemtype in elemtypes

    p0 = Point(0.1,0.2,0.3)
    p1 = Point(-0.4,-0.5,-0.6)
    nx = 2
    ny = 3
    nz = 4
    mesh = meshbox(p0, p1, nx, ny, nz, elemtype)

    # Test elements
    if elemtype == Hex8
        @test size(mesh.elements) == (nx*ny*nz, 8)
    elseif elemtype == Tet4
        @test size(mesh.elements) == (6*nx*ny*nz, 4)
    end

    # Test nodes
    @test length(mesh.nodes) == 60

    # Test nsets
    @test all(isapprox.(getindex.(mesh.coordinates[mesh.nsets["xmin"]], 1), p1[1]))
    @test all(isapprox.(getindex.(mesh.coordinates[mesh.nsets["ymid"]], 2), (p1[1] - p0[1])/2))
    @test all(isapprox.(getindex.(mesh.coordinates[mesh.nsets["zmax"]], 3), p0[3]))
end
