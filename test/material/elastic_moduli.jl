using SofTiSim

@test_throws ArgumentError elastic_moduli(E=1)
@test_throws ArgumentError elastic_moduli(E=1, nu=2, G=3)

E = 3000
nu = 0.49
em = elastic_moduli(E=E, nu=nu)

"""
    test_dict_isapprox(a, b)

Test that the two `Dict`s `a` and `b` have the same keys
and approximately the same values.
"""
function test_dict_isapprox(a, b)
    @test length(a) == length(b)
    for ((key_a, value_a), (key_b, value_b)) in zip(pairs(a), pairs(b))
        if !(key_a === key_b) || !(value_a ≈ value_b)
            @error "Comparison failed: ($key_a => $value_a) != ($key_b => $value_b)"
        end
        @test key_a === key_b
        @test value_a ≈ value_b
    end
end

test_dict_isapprox(em, elastic_moduli(K=em[:K], E=em[:E]))
test_dict_isapprox(em, elastic_moduli(K=em[:K], lambda=em[:lambda]))
test_dict_isapprox(em, elastic_moduli(K=em[:K], G=em[:G]))
test_dict_isapprox(em, elastic_moduli(K=em[:K], nu=em[:nu]))
test_dict_isapprox(em, elastic_moduli(K=em[:K], M=em[:M]))
test_dict_isapprox(em, elastic_moduli(E=em[:E], lambda=em[:lambda]))
test_dict_isapprox(em, elastic_moduli(E=em[:E], G=em[:G]))
test_dict_isapprox(em, elastic_moduli(E=em[:E], nu=em[:nu]))
test_dict_isapprox(em, elastic_moduli(E=em[:E], M=em[:M]))
test_dict_isapprox(em, elastic_moduli(lambda=em[:lambda], G=em[:G]))
test_dict_isapprox(em, elastic_moduli(lambda=em[:lambda], nu=em[:nu]))
test_dict_isapprox(em, elastic_moduli(lambda=em[:lambda], M=em[:M]))
test_dict_isapprox(em, elastic_moduli(G=em[:G], nu=em[:nu]))
test_dict_isapprox(em, elastic_moduli(G=em[:G], M=em[:M]))
test_dict_isapprox(em, elastic_moduli(nu=em[:nu], M=em[:M]))

@test_throws ArgumentError elastic_moduli(moo=1, meow=2)
@test_throws ArgumentError elastic_moduli(lambda=1, nu=0)
@test_throws ArgumentError elastic_moduli(lambda=0, nu=1)
