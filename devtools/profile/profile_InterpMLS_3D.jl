using Profile, ProfileView
Profile.clear()

@time @profile for qp in glob.qpoints
    nodes, phi, dphidx = InterpMLS_3D_temp(
        glob.coord,
        glob.support_radius,
        qp.coordinates,
        model[:use_exact_SF_derivatives])
end

Profile.print(maxdepth=9)

ProfileView.view()
