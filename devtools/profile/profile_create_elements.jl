using BenchmarkTools
using Profile, ProfileView

Profile.clear()
@profile create_elements(glob.coord, mesh.elements);
Profile.print(maxdepth=8)
ProfileView.view()

@benchmark create_elements(glob.coord, mesh.elements)


@time cells = create_elements(glob.coord, mesh.elements);
