# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

module TicToc
"""
Wall-clock timer functions tic and toc

Usage:

using TicToc

tic("an optional message")
toc("an optional message")
"""

export tic, toc

using Printf

global _start_time
global _lap_time

"""
    tic()

Start wall-clock timer.
"""
function tic()
    global _start_time
    global _lap_time
    _start_time = time()
    _lap_time = time()
    return nothing
end

"""
    toc(msg)

Measure elapsed time on wall-clock timer.
"""
function toc(msg)
    global _start_time
    global _lap_time
    elapsed = @sprintf " (Time elapsed: %.2f s, lap: %.2f s)" _time() _lap()
    return join([msg, elapsed])
end

function toc()
    global _start_time
    return elapsed = @sprintf "Time elapsed: %.2f s, lap: %.2f s" _time() _lap()
end

function _lap()
    global _start_time
    global _lap_time
    t = time() - _lap_time
    _lap_time = time()
    return t
end

function _time()
    global _start_time
    return time() - _start_time
end

function test()
    println("Start timing")
    tic()
    sleep(rand())
    println(toc())
    sleep(rand())
    println(toc("An optional message"))
    sleep(rand())
    toc()
end

end
