# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using StaticArrays

"""
    eltocols(v::Vector{SVector})

View elements of the `SVector` `v` as columns of a matrix.
"""
eltocols(v::AbstractVector{SVector{Dim, T}}) where {Dim, T} =
    reshape(reinterpret(T, v), Dim, :)

"""
    eltorows(v::Vector{SVector})

View elements of the `SVector` `v` as rows of a matrix.
"""
eltorows(v::AbstractVector{SVector{Dim, T}}) where {Dim, T} =
    transpose(reshape(reinterpret(T, v), Dim, :))

"""
    eltocols(v::Vector{Vector})

View elements of  the `Vector` `v` as columns of a matrix.
This will fail if not all elements have the same length.
"""
eltocols(v::AbstractVector{Vector{T}}) where {T} =
    collect(reduce(hcat, v))

"""
    eltorows(v::Vector{Vector})

View elements of  the `Vector` `v` as rows of a matrix.
This will fail if not all elements have the same length.
"""
eltorows(v::AbstractVector{Vector{T}}) where {T} =
    collect(transpose(reduce(hcat, v)))
