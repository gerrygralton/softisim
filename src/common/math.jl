# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using LinearAlgebra

"""
    triple_scalar_product(a, b, c)

Return the triple scalar product
(also called the scalar triple product, mixed product or box product)
of `a`, `b` and `c`.
"""
triple_scalar_product(a, b, c) = (a[1]*(b[2]*c[3] - b[3]*c[2]) -
                                  a[2]*(b[1]*c[3] - b[3]*c[1]) +
                                  a[3]*(b[1]*c[2] - b[2]*c[1]))

"""
    triple_vector_product(a, b, c)

Return the triple vector product
(also called the vector triple product)
of `a`, `b` and `c`.
"""
triple_vector_product(a, b, c) = cross(a, cross(b, c))
