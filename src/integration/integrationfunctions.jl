# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

import ProgressMeter

"""
    mass_scaling_factors!(qpoints, mass_scaling_timestep)

Compute mass scaling factor
for each `qp` in `qpoints`.
"""
function mass_scaling_factors(
    critical_time_steps::Vector{Float64},
    mass_scaling_timestep::Function
)::Vector{Float64}

    # Global stable time step size
    DT = mass_scaling_timestep(critical_time_steps)::Float64

    # Mass scaling factor for each point
    return [max(1., (DT/dt)^2) for dt in critical_time_steps]
end

"""
    critical_time_step(num_nodes, wave_speed, mass_scaling_factor, derivatives)

Compute the critical time step (meshless) given the
`num_nodes` in the support,
`wave_speed`,
`mass_scaling_factor`, and
matrix of shape function `derivatives`.
"""
function critical_time_step(
    num_nodes::Int,
    wave_speed::Float64,
    mass_scaling_factor::Float64,
    derivatives::Matrix{Float64}
)::Float64

    dt_critical = 2. / (wave_speed * sqrt(num_nodes * sum(derivatives.^2)))
    dt_critical *= sqrt(mass_scaling_factor)
end

"""
    critical_time_step(qp)

Compute critical time step (meshless) for integration point `qp`.
"""
function critical_time_step(
    qp::QuadraturePoint{Dim}
)::Vector{Float64} where Dim

    dt_critical = zeros(length(qp))

    for i = 1:length(qp)
        dt_critical[i] = critical_time_step(length(qp.nodes[i]),
                                            qp.wave_speed[i],
                                            qp.mass_scaling_factor[i],
                                            qp.derivatives[i])
    end
    return dt_critical
end

"""
Return a vector of `QuadraturePoint`s.
"""
function create_qpoints(
    centers::Vector{Point{Dim}},
    support::Vector{Float64},
    cells::Vector{E},
    model::Dict,
    InterpMLS_3D_temp::Union{Nothing, Function}, # FIXME: not nothing!
    approx::ApproxOrInterp
)::QuadraturePoint where {Dim, E<:Elem}

    qpoints = QuadraturePoint{Dim}()

    num_elements = length(cells)
    @info string("Number of elements:          ", num_elements)

    if SHOW_PROGRESS_METER
        progress = ProgressMeter.Progress(num_elements, desc="Coordinates and weights: ")
    end
    # FIXME: use actual element numbers instead here to fix this bug:
    @warn "FIXME: element numbers must be continuous and begin at 1"
    # for (id, cell) in zip(element_ids, cells)
    @time for (id, cell) in enumerate(cells)
        if is_meshfree(model[:approx]) && model[:use_adaptive_integration]
            integral = gauss_tetra4

            f1(x) = ShapeFuncDerNorm(
                centers,
                support,
                x,
                model[:use_exact_SF_derivatives],
                model[:basic_function],
                InterpMLS_3D_temp)

            coord_vec, weight_vec = adaptive_tetra_divisions(
                f1, integral, cell,
                model[:integration_eps],
                model[:number_of_tetrahedral_divisions])
        else
            coord_vec, weight_vec = gauss(cell, model[:quadrature_degree])
        end
        for (coord, weight) in zip(coord_vec, weight_vec)
            push!(qpoints.coordinates, coord)
            # FIXME: is there a better way to do this?
            push!(qpoints.JxW, volume(cell) / reference_volume(cell) * weight)
            # FIXME: element numbers must be continuous and begin at 1
            push!(qpoints.element_id, id)
        end
        if SHOW_PROGRESS_METER
            ProgressMeter.next!(progress)
        end
    end

    num_qpoints = length(qpoints)
    @info string("Number of quadrature points: ", num_qpoints)

    # FIXME: allocate these vectors more efficiently
    qpoints.nodes = Vector{Vector{Int}}(undef, num_qpoints)
    qpoints.derivatives = Vector{Matrix{Float64}}(undef, num_qpoints)

    if SHOW_PROGRESS_METER
        progress = ProgressMeter.Progress(num_qpoints, desc="Basis and derivatives: ")
    end
    @time Threads.@threads for qp = 1:num_qpoints
        if is_meshfree(approx) # TODO: dispatch on the type of approximation i.e. MLS or FE
            # NOTE: MLS needs the global coordinates so map them before computing derivatives
            qpoints.coordinates[qp] = map_from_reference_cell(
                cells[qpoints.element_id[qp]], qpoints.coordinates[qp])
            # FIXME: define nodes_shape_value_and_deriv for meshfree
            nodes, _, derivatives = InterpMLS_3D_temp(
                centers,
                support,
                qpoints.coordinates[qp],
                model[:use_exact_SF_derivatives],
                model[:basic_function])
        else
            nodes, _, derivatives = nodes_shape_value_and_deriv(
                approx,
                # FIXME: add qpoints.elements[qp] to FEValues as a pointer to mesh element
                cells[qpoints.element_id[qp]],
                qpoints.coordinates[qp])
            # NOTE: FEM needs the reference coordinates so map them after computing derivatives
            # FIXME: probably better to store elements instead and compute coords and weights
            #        as needed by using a QuadratureRule type attached to each element.
            #        These coords are probably not used for FEM expect maybe for visualisation?
            qpoints.coordinates[qp] = map_from_reference_cell(
                cells[qpoints.element_id[qp]], qpoints.coordinates[qp])
        end

        qpoints.nodes[qp] = nodes
        qpoints.derivatives[qp] = derivatives

        if SHOW_PROGRESS_METER
            ProgressMeter.next!(progress)
        end
    end

    return qpoints
end
