# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
Integration point
"""
mutable struct QuadraturePoint{Dim}
    """Element associated with this integration point."""
    element_id::Vector{Int64}

    """Section associated with this integration point."""
    section_id::Vector{Int64}

    """Coordinates of this integration point."""
    coordinates::Vector{Point{Dim}}

    """Mapped quadrature weight"""
    JxW::Vector{Float64}

    """Nodes within the support of this point (connectivity)"""
    nodes::Vector{Vector{Int}}

    # TODO: make this a Vector{Point{Dim}}?
    # or an SMatrix? No, size not known at compile (disadvantage of meshfree)
    # pros: faster? Dim is known at compile time
    # cons: cannot do matrix operations e.g. B'*S
    """Shape function derivatives"""
    derivatives::Vector{Matrix{Float64}}

    """Density"""
    density::Vector{Float64}

    """Material"""
    material::Vector{Material}

    """Material wave speed"""
    wave_speed::Vector{Float64}

    """Critical time step"""
    critical_time_step::Vector{Float64}

    """Mass scaling factor"""
    mass_scaling_factor::Vector{Float64}

    """Strain energy"""
    strain_energy::Vector{Float64}

    """Displacement gradient H (not transposed)
    Note the following relations:
    F = I + H
    C = F'*F
    B = F*F'
    E = 1/2(C-I)
    Also, note that in the assembly code the transpose gradient HT is used.
    """
    gradient::Vector{MMatrix{Dim, Dim, Float64}}

    function QuadraturePoint{Dim}() where Dim
        qpoints = new{Dim}()

        qpoints.element_id = Int64[]
        qpoints.section_id = Int64[]
        qpoints.coordinates = Point{Dim}[]
        qpoints.JxW = Float64[]
        qpoints.nodes = Vector{Vector{Int}}()
        qpoints.derivatives = Vector{Matrix{Float64}}()
        qpoints.density = Float64[]
        qpoints.material = Material[]
        qpoints.wave_speed = Float64[]
        qpoints.critical_time_step = Float64[]
        qpoints.mass_scaling_factor = Float64[]
        qpoints.strain_energy = Float64[]
        qpoints.gradient = Vector{MMatrix{Dim, Dim, Float64}}()

        return qpoints
    end
end

dim(::QuadraturePoint{Dim}) where Dim = Dim

Base.length(collection::QuadraturePoint) = length(collection.coordinates)
