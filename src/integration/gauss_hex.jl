# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

# TODO: Dispatch on e.g. ::Type{Hex} instead of Hex
#       because these QuadratureRules are defined for
#       reference elements.

# TODO: turn QuadratureRule into a struct later;
#       but leave for now because other code uses tuples.
"""
Type alias for quadrature rules
"""
QuadratureRule{Dim} = NamedTuple{
    (:p, :w),
    Tuple{Vector{Point{Dim}}, Vector{Float64}}
} where Dim

"""
    gauss(::Hex, degree)

Gauss quadrature point coordinates and weights for hexahedron.
"""
function gauss(::Hex, degree::Integer)::QuadratureRule{3}

    # Compute the 3D quadrature rule as a tensor product
    # of the 1D quadrature rule.
    qr1d = gauss(Edge, degree)
    return tensor_product_hex(qr1d)
end

"""
    gauss(::Type{Edge}, degree)

Gauss quadrature point coordinates and weights for edge or line segment.
"""
function gauss(::Type{Edge}, degree::Int)::QuadratureRule{1}

    if degree >= 0 && degree <= 1 # 1 point

        points = [Point(0.)]

        weights = [2.]

    elseif degree <= 3 # 2 points

        points = [Point(-sqrt(3)/3),
                  Point(sqrt(3)/3)]

        weights = ones(2)

    elseif degree <= 5 # 3 points

        points = [Point(-sqrt(3/5)),
                  Point(0.),
                  Point(sqrt(3/5))]

        weights = [5/9, 8/9, 5/9]

    else
        error("Quadrature rule of degree ", degree, " not supported!")
    end

    return (p = points, w = weights)
end

"""
    tensor_product_hex(qrx, qry, qrz)

Returns the tensor product quadrature rule [qrx x qry x qrz]
from the 1D rules qrx, qry and qrz.
"""
function tensor_product_hex(
    qrx::QuadratureRule{1},
    qry::QuadratureRule{1},
    qrz::QuadratureRule{1}
)::QuadratureRule{3}
    # See: libMesh/src/quadrature/quadrature.C; MFEM/fem/intrules.cpp

    nx = length(qrx.p)
    ny = length(qry.p)
    nz = length(qrz.p)
    np = nx * ny * nz

    points  = Vector{Point{3}}(undef, np)
    weights = Vector{Float64}(undef, np)

    q = 1
    for iz = 1:nz
        pz = qrz.p[iz][1]
        wz = qrz.w[iz]
        for iy = 1:ny
            py = qry.p[iy][1]
            wy = qry.w[iy]
            for ix = 1:nx
                px = qrx.p[ix][1]
                wx = qrx.w[ix]
                points[q] = Point{3}(px, py, pz)
                weights[q] = wx * wy * wz
                q += 1
            end
        end
    end

    return (p = points, w = weights)
end

"""
    tensor_product_hex(qr)

Returns the tensor product quadrature rule [qr x qr x qr]
from the 1D rule qr.
"""
function tensor_product_hex(qr::QuadratureRule{1})::QuadratureRule{3}
    tensor_product_hex(qr, qr, qr)
end
