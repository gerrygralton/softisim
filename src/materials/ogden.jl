# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using LinearAlgebra
using StaticArrays

"""
Ogden material.
"""
struct Ogden <: Material
    "Material parameter α"
    alpha::Float64

    "Initial shear modulus μ (Lamé's second parameter)"
    mu::Float64

    "Initial bulk modulus κ"
    kappa::Float64

    "If evaluation point has zero volume (negative Jacobian determinant):
    `:terminate` analysis (default),
    `:continue` with zero or negative volume, or
    `:delete` this point (set deformation gradient to zero)"
    when_zero_volume::Symbol

    "Default constructor"
    function Ogden(alpha::Number, mu::Number, kappa::Number; when_zero_volume = :terminate)
        new(alpha, mu, kappa, when_zero_volume)
    end
    "Constructor using elastic moduli from dictionary"
    function Ogden(alpha::Number, elastic_moduli::NamedTuple; when_zero_volume = :terminate)
        mu = elastic_moduli[:G]
        kappa = elastic_moduli[:K]
        @info string("Creating Ogden material ",
                     "(μ: ", mu, ", κ: ", kappa, ")")
        new(alpha, mu, kappa, when_zero_volume)
    end
end

function wave_speed(density, material::Ogden)
    return sqrt((material.kappa + 4/3*material.mu)/density)
end

"""
    spkstress(FT, material::Ogden, data)

Compute second Piola--Kirchoff stress for a Ogden material.
"""
function spkstress(FT::SMatrix{3, 3, Float64, 9},
                   material::Ogden,
                   data)

    # Right Cauchy-Green deformation tensor
    C = FT * FT'

    # Ogden model parameters
    alpha = material.alpha
    mu = material.mu
    kappa = material.kappa

    # Eigenvalues and eigenvectors
    Eigs, Dirs = eigen(C)

    # Principal stretches
    lamda1 = sqrt(Eigs[1])
    lamda2 = sqrt(Eigs[2])
    lamda3 = sqrt(Eigs[3])

    # Strain energy function derivatives
    J = lamda1 * lamda2 * lamda3
    Slambda = lamda1^alpha + lamda2^alpha + lamda3^alpha
    b = 2 * mu / alpha * J^(-alpha / 3)
    a = kappa * J * (J - 1) - b/3 * Slambda

    dUdl1 = a / lamda1 + b * lamda1^(alpha - 1)
    dUdl2 = a / lamda2 + b * lamda2^(alpha - 1)
    dUdl3 = a / lamda3 + b * lamda3^(alpha - 1)

    # Second Piola-Kirchhoff stress tensor
    S_principal = SVector{3, Float64}([dUdl1/lamda1, dUdl2/lamda2, dUdl3/lamda3])
    S = SMatrix{3, 3, Float64}(Dirs * Diagonal(S_principal) * inv(Dirs))

    return S
end

"""
    strain_energy(FT, material)

Compute strain energy for an Ogden material.
"""
function strain_energy(FT::SMatrix{3, 3, Float64, 9},
                        material::Ogden)

    J = det(FT)         # Determinant of deformation gradient

    C = FT*FT'          # Right Cauchy-Green deformation tensor
    stretches = sqrt.(eigen(C).values) # Principal stretches

    mu = material.mu     # Ogden material parameters
    alpha = material.alpha
    kappa = material.kappa

    I1 = J^(-alpha/3) * sum(stretches)
    return  (2*mu) / (alpha^2) * (I1 - 3) + kappa * (J-1)^2    # Eq. 3.55 (Zwick, 2020)
end
