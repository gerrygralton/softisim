# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using StaticArrays

"""
Neo-Hookean material.
"""
struct NeoHookean <: Material
    "Initial shear modulus μ (Lamé's second parameter)"
    mu::Float64

    "Initial bulk modulus κ"
    kappa::Float64

    "If evaluation point has zero volume (negative Jacobian determinant):
    `:terminate` analysis (default),
    `:continue` with zero or negative volume, or
    `:delete` this point (set deformation gradient to zero)"
    when_zero_volume::Symbol

    "Default constructor"
    function NeoHookean(mu::Number, kappa::Number; when_zero_volume = :terminate)
        new(mu, kappa, when_zero_volume)
    end

    "Constructor using elastic moduli from dictionary"
    function NeoHookean(elastic_moduli::NamedTuple; when_zero_volume = :terminate)
        mu = elastic_moduli[:G]
        kappa = elastic_moduli[:K]
        @info string("Creating neo-Hookean material ",
                     "(μ: ", mu, ", κ: ", kappa, ")")
        new(mu, kappa, when_zero_volume)
    end
end

function wave_speed(density, material::NeoHookean)
    return sqrt((material.kappa + 4/3*material.mu)/density)
end

function spkstress(FT::SMatrix{3, 3, Float64, 9},
                   material::NeoHookean,
                   data)
    mu = material.mu
    kappa = material.kappa

    # FT is pointer to F transposed
    # Assign input data
    dg11 = FT[1,1]; dg12 = FT[1,2]; dg13 = FT[1,3]
    dg21 = FT[2,1]; dg22 = FT[2,2]; dg23 = FT[2,3]
    dg31 = FT[3,1]; dg32 = FT[3,2]; dg33 = FT[3,3]
    # Determinant of deformation gradient
    J = (dg11*(dg22*dg33 - dg32*dg23) +
         dg12*(dg23*dg31 - dg21*dg33) +
         dg13*(dg21*dg32 - dg22*dg31))
    # Right Cauchy Green matrix = FT * FT'
    rc11 = dg11*dg11 + dg12*dg12 + dg13*dg13
    rc12 = dg11*dg21 + dg12*dg22 + dg13*dg23
    rc13 = dg11*dg31 + dg12*dg32 + dg13*dg33
    rc22 = dg21*dg21 + dg22*dg22 + dg23*dg23
    rc23 = dg21*dg31 + dg22*dg32 + dg23*dg33
    rc33 = dg31*dg31 + dg32*dg32 + dg33*dg33
    # compute J^(-2/3) using 4 terms from Taylor expansion
    dg11 = (J - 1)/3
    dg12 = 5*dg11*dg11
    dg22 = 1 - 2*dg11 + dg12 - (8/3)*dg12*dg11
    # x = (kappa*J*(J-1)- mu*(J^-2/3)*I1/3)/(J^2)
    x = J*(J-1)*kappa - mu*dg22*(rc11+rc22+rc33)/3
    x /= (J*J)
    dg22 *= mu
    S = @MMatrix zeros(Float64, 3, 3)
    S[1,1] = (rc22*rc33 - rc23*rc23) * x + dg22
    S[2,2] = (rc11*rc33 - rc13*rc13) * x + dg22
    S[3,3] = (rc11*rc22 - rc12*rc12) * x + dg22
    S[1,2] = (rc13*rc23 - rc12*rc33) * x
    S[2,3] = (rc13*rc12 - rc11*rc23) * x
    S[1,3] = (rc12*rc23 - rc13*rc22) * x
    S[2,1] = S[1,2]
    S[3,2] = S[2,3]
    S[3,1] = S[1,3]

    return SMatrix(S)
end

"""
    strain_energy(FT, material)

Compute strain energy for a neo-Hookean material.
"""
function strain_energy(FT::SMatrix{3, 3, Float64, 9},
                        material::NeoHookean)

    J = det(FT)         # Determinant of deformation gradient

    C = FT*FT'          # Right Cauchy-Green deformation tensor
    I1 = J^(-2/3) * tr(C)   #First deviatoric strain invariant

    return 0.5 *(material.mu * (I1 - 3) + material.kappa * (J - 1)^2) # Eq. 3.52 (Zwick, 2020)
end
