# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

print("Importing Makie... ")
import Makie
println("done.")
# Should the display not work after compilation, use
# AbstractPlotting.__init__(), or force display by calling
# display(AbstractPlotting.PlotDisplay(), scene); on your Scene.
# See: https://github.com/JuliaPlots/Makie.jl
Makie.AbstractPlotting.__init__()

# Undeformed object
scene = Makie.mesh(x2, faces, color=(:white, 0.5), shading=false)
Makie.wireframe!(scene, color = (:black, 0.6), linewidth = 3)
Makie.scatter!(scene, x2, color=:blue, markersize=0.005)

# Deformed object
scene = Makie.mesh(x_int, faces, color=(:white, 0.5), shading=false)
Makie.wireframe!(scene[end][1], color=(:black, 0.6), linewidth=3)
Makie.scatter!(x_int, color=:blue, markersize=0.005)

# Convergence rate
Makie.lines(1:stepnum_total, results.ro_time[1:stepnum_total])
Makie.lines!(1:stepnum_total, results.ro_used_time[1:stepnum_total])
# title("Convergence rate")
# xlabel("Iteration")
# legend("Estimated", "Used")

# Maximum displacement variation
Makie.lines(1:stepnum_total, results.Max_disp_variation_time[1:stepnum_total])
# title("Maximum displacement variation")
# xlabel("Iteration")

# Displacement vectors (quiver)
arrows([Point3f0.(x[1], x[2], x[3]) for x in eachrow(x2)],
       [Point3f0.(u[1], u[2], u[3]) for u in eachrow(results.u_t2)],
       arrowsize=0.5)

# Displacement magnitude on deformed object
color = sqrt.(u_int[:,1].^2 + u_int[:,2].^2 + u_int[:,3].^2);
scene = Makie.mesh(x_int, faces, color=color, shading=false)
Makie.wireframe!(scene[end][1], color = (:black, 0.6), linewidth = 3)
Makie.mesh!(x2, faces, color = (:white, 1.0), linewidth = 3, shading=false)
Makie.wireframe!(scene[end][1], color = (:black, 0.6), linewidth = 3)
