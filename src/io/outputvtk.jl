# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Printf: @sprintf
import WriteVTK

mutable struct OutputFieldVtk <: OutputField
    filename::String
    interval::Int
    variables::Set{String}

    pvdfile::WriteVTK.CollectionFile
    coordinates::Array{Float64,2}
    cells::Array{WriteVTK.MeshCell,1}

    disp_exact_function::Union{Function, Nothing}

    function OutputFieldVtk(;
                       interval::Int,
                       variables=["dnod_curr",
                                  "dnod_next",
                                  "disp",
                                  "forc_tot",
                                  "forc_ebc"])

        output = new()
        output.interval = interval
        output.variables = Set{String}(variables)
        return output
    end
end

function initialise_output!(output::OutputFieldVtk;
                            filename::String,
                            mesh,
                            disp_exact_function::Union{Function,Nothing} = nothing)

    output.filename = filename * ".field"
    output.pvdfile = WriteVTK.paraview_collection(output.filename)

    output.coordinates = eltocols(mesh.coordinates)

    # FIXME: this does not support mesh with mixed cell types
    num_nodes_per_element = size(mesh.elements, 2)
    if num_nodes_per_element == 4
        celltype = WriteVTK.VTKCellTypes.VTK_TETRA
    elseif num_nodes_per_element == 8
        celltype = WriteVTK.VTKCellTypes.VTK_HEXAHEDRON
    elseif num_nodes_per_element == 10
        celltype = WriteVTK.VTKCellTypes.VTK_QUADRATIC_TETRA
    elseif num_nodes_per_element == 20
        celltype = WriteVTK.VTKCellTypes.VTK_QUADRATIC_HEXAHEDRON
    else
        throw(ArgumentError("Unsupported number of nodes per element: $num_nodes_per_element"))
    end

    output.cells = WriteVTK.MeshCell[]
    for i=1:size(mesh.elements, 1)
        push!(output.cells,
              WriteVTK.MeshCell(celltype, mesh.elements[i,:]))
    end

    # User-defined exact value function for displacements
    if !(disp_exact_function isa Nothing)
        output.disp_exact_function = disp_exact_function
        push!(output.variables, "disp_exact")
        push!(output.variables, "disp_error_absolute")
        push!(output.variables, "disp_error_relative")
    else
        output.disp_exact_function = nothing
    end
end

function write_output(output::OutputFieldVtk, glob::GlobalVariables)

    vtkfile = WriteVTK.vtk_grid(@sprintf("%s-%06i.vtu", output.filename, glob.increment),
                                output.coordinates, output.cells)

    results = Dict(
        "dnod_curr" => (glob.dnod_curr[:,1], glob.dnod_curr[:,2], glob.dnod_curr[:,3]),
        "dnod_next" => (glob.dnod_next[:,1], glob.dnod_next[:,2], glob.dnod_next[:,3]),
        "disp"      => (glob.disp[:,1], glob.disp[:,2], glob.disp[:,3]),
        "forc_tot"  => (glob.forc_tot[:,1], glob.forc_tot[:,2], glob.forc_tot[:,3]),
        "forc_ebc"  => (glob.forc_ebc[:,1], glob.forc_ebc[:,2], glob.forc_ebc[:,3]))

    for k in keys(results)
        if k in output.variables
            WriteVTK.vtk_point_data(vtkfile, results[k], k)
        end
    end

    if !(output.disp_exact_function isa Nothing)
        uh = glob.disp                         # approximated displacement
        ue = output.disp_exact_function(glob)  # exact displacement
        ea = abs.(uh .- ue)                    # absolute error
        er = ea ./ abs.(ue)                    # relative error
        # Could avoid divide by zero (see below) but better to plot NaNs in ParaView
        # er = ea ./ uh                          # relative error
        # er = ea ./ max.(abs.(eltocols(glob.coord)), abs.(ue)) # relative error - See Hamming book
        WriteVTK.vtk_point_data(vtkfile, (ue[:,1], ue[:,2], ue[:,3]), "disp_exact")
        WriteVTK.vtk_point_data(vtkfile, (ea[:,1], ea[:,2], ea[:,3]), "disp_error_absolute")
        WriteVTK.vtk_point_data(vtkfile, (er[:,1], er[:,2], er[:,3]), "disp_error_relative")
    end

    WriteVTK.vtk_save(vtkfile)

    WriteVTK.collection_add_timestep(output.pvdfile, vtkfile, glob.increment)
end

function finish(output::OutputFieldVtk)
    WriteVTK.vtk_save(output.pvdfile)
end
