# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    smooth345(t)

Smooth 3-4-5 polynomial amplitude curve.
"""
function smooth345(t)
    if t <= 0
        return 0
    elseif t >= 1
        return 1
    else
        return 10t^3 - 15t^4 + 6t^5
    end
end
