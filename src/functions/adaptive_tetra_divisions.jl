# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    adaptive_tetra_divisions(f_handle, integration_f_handle, cell, accuracy, num_divisions)
"""
function adaptive_tetra_divisions(
    f_handle::Function,
    integration_f_handle::Function,
    cell::Tet4,
    accuracy::Float64,
    num_divisions::Integer)

    # Original integral value, quadrature points and weights
    I_old, Q, JxW = integration_f_handle(f_handle, cell)

    # New cells
    # these temporary cells do not need nodes
    nodes = zeros(Int8, 4)

    # Original points
    P1 = cell.point[1]
    P2 = cell.point[2]
    P3 = cell.point[3]
    P4 = cell.point[4]

    if num_divisions == 2
        # 2 tetrahedral sub-division
        # find the coordinates of the new set of nodes
        # (4 old plus 1 new)
        P5 = (P1 + P2)/2

        cells_new = [
            Tet4(nodes, [P1, P5, P3, P4]),
            Tet4(nodes, [P2, P3, P5, P4]),
        ]

    elseif num_divisions == 4
        # 4 tetrahedral sub-division
        # find the coordinates of the new set of nodes
        # (4 old plus 3 new)
        P5 = (P1 + P4)/2
        P6 = (P4 + P3)/2
        P7 = (P3 + P1)/2

        cells_new = [
            Tet4(nodes, [P1, P5, P7, P2]),
            Tet4(nodes, [P5, P6, P7, P2]),
            Tet4(nodes, [P3, P7, P6, P2]),
            Tet4(nodes, [P5, P4, P6, P2]),
        ]

    elseif num_divisions == 8
        # 8 tetrahedral sub-division
        # find the coordinates of the new set of nodes
        # (4 old plus 6 new)
        P5 = (P1 + P2)/2
        P6 = (P2 + P4)/2
        P7 = (P2 + P3)/2
        P8 = (P1 + P4)/2
        P9 = (P3 + P4)/2
        P10 = (P1 + P3)/2

        cells_new = [
            Tet4(nodes, [P1, P5, P10, P8]),
            Tet4(nodes, [P2, P5, P6, P7]),
            Tet4(nodes, [P3, P7, P9, P10]),
            Tet4(nodes, [P4, P6, P8, P9]),
            Tet4(nodes, [P5, P6, P9, P8]),
            Tet4(nodes, [P6, P5, P9, P7]),
            Tet4(nodes, [P7, P9, P10, P5]),
            Tet4(nodes, [P8, P10, P9, P5]),
        ]

    else
        throw(ArgumentError("Unsupported number of divisions: $num_divisions"))
    end

    # New integral value
    I_new = 0.
    for c in cells_new
        I_new += integration_f_handle(f_handle, c)[1]
    end

    err = abs(1. - I_new/I_old)
    # DEBUG:
    # println("I_new: ", I_new)
    # println("I_old: ", I_old)
    # println("err: ", err)

    if err > accuracy
        # New quadrature points and weights
        Q = Point{3}[]
        JxW = Float64[]
        for c in cells_new
            Qc, JxWc = adaptive_tetra_divisions(f_handle, integration_f_handle, c, accuracy, num_divisions)
            append!(Q, Qc)
            append!(JxW, JxWc)
        end
    end

    return Q, JxW
end
