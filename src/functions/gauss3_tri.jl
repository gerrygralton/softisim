# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

function gauss3_tri(x)

    coordinates = zeros(3, 2)
    weights     = zeros(3, 1)

    coordinates[1,:] = 2/3*x[1,:] + 1/6*x[2,:] + 1/6*x[3,:]
    coordinates[2,:] = 1/6*x[1,:] + 2/3*x[2,:] + 1/6*x[3,:]
    coordinates[3,:] = 1/6*x[1,:] + 1/6*x[2,:] + 2/3*x[3,:]

    area_el_disp = (x[2,1]*x[3,2]-
                    x[3,1]*x[2,2]+
                    x[3,1]*x[1,2]-
                    x[1,1]*x[3,2]+
                    x[1,1]*x[2,2]-
                    x[2,1]*x[1,2])

    area_el_disp = area_el_disp/2

    weights[1,:] .= area_el_disp/3
    weights[2,:] .= area_el_disp/3
    weights[3,:] .= area_el_disp/3

    return coordinates, weights
end
