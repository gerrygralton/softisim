# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    gauss_tetra4(f_handle, nodes_coord)

Integrate using 4-point Gauss quadrature over tetrahedron.
"""
function gauss_tetra4(f_handle::Function, cell::Tet4)

    Q, W = gauss(cell, 2)
    @assert length(Q) == 4
    @assert length(W) == 4

    # FIXME: should not need to use abs here
    #        (some nodes are not ordered right in adaptive
    #         that cause negative volume and infinite loop?)
    JxW = abs(volume(cell)) / reference_volume(cell) * W

    # Use global coordinates q to evaluate function but
    # return reference coordinates Q
    q = Vector{Point{3}}(undef, length(Q))
    for i = 1:length(Q)
        q[i] = map_from_reference_cell(cell, Q[i])
    end

    val = sum([JxW JxW JxW].*f_handle(q))

    return val, Q, JxW
end
