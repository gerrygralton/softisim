# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

# TODO: rename this file as initialize_output.jl to match function name
#       or add more functions and leave filename as is.

import Dates
import LibGit2
import Logging
import Memento

"""
    initialize_output(output_directory, overwrite_output)

Create output directory, log file, etc. and return the output filename prefix.
"""
function initialize_output(
    output_directory::String,
    overwrite_output::Bool, # "Overwrite output directory and files without asking"
)::String

    # Output directory and file names
    if output_directory == ""
        output_filename = ""
    else
        # Convert given path to absolute path
        output_directory = abspath(output_directory)

        # Create output directory
        if ispath(output_directory)
            if overwrite_output
                printstyled("Overwriting output directory:\n",
                            output_directory, "\n", color=:yellow)
            else
                printstyled("Output directory:\n",
                            output_directory, "\n", color=:red)
                printstyled("exists. Overwrite y/[n]? ", color=:red)
                choice = readline()
                if !(lowercase(choice) == "y")
                    error("Output directory exists and user chose not to overwrite it.")
                end
            end
        else
            mkdir(output_directory)
        end

        # Prefix prepended to all output files
        output_filename = joinpath(output_directory, basename(output_directory))

        # ----------------------------------------------------------------------
        # TODO:
        # Serialize the model and solver (not mesh)
        # TODO: First split field/history_output parameters and data
        #       otherwise this will be huge!
        # TODO: print the model dict and solver parameters to echo instead
        #       (there is no input file now)
        # # Copy (echo) input file to output directory for future reference
        # cp(abspath(PROGRAM_FILE), output_filename * ".echo", force=true)
        # ----------------------------------------------------------------------

        # Log to file
        log_filename = output_filename * ".log"
        # Write something before beginning the logging in case there is an error
        open(log_filename, "w") do io
            println(io, "SofTiSim: ", SOFTISIM_VERSION_STRING)
            println(io, "Julia: ", VERSION)
            println(io, "Start logging...")
        end
        # Use Memento for all logging
        Memento.reset!()   # otherwise log files are appended to handler!
        Memento.substitute!()
        # console log handler
        push!(Memento.getlogger(), Memento.DefaultHandler(
            stdout,
            Memento.DefaultFormatter(),
            Dict(:is_colorized => true,
                 :colors => Dict{AbstractString, Symbol}(
                     "debug" => :blue,
                     "info" => :normal, # green
                     "notice" => :cyan,
                     "warn" => :magenta,
                     "error" => :red,
                     # "critical" => :yellow,
                     # "alert" => :white,
                     # "emergency" => :black,
                 ))
        ))
        # file log handler
        push!(Memento.getlogger(), Memento.DefaultHandler(
            log_filename
        ))
    end

    @info string("Start time: ", string(Dates.now()))
    @info string("Job name: ", basename(output_filename))
    @info string("Output directory: ", dirname(output_filename))
    @info string("Number of threads: ", Threads.nthreads())

    # Reset global flags
    global WARN_SMALL_VOLUME = true
    global WARN_ZERO_VOLUME = true
    @debug "WARN_SMALL_VOLUME: $WARN_SMALL_VOLUME"
    @debug "WARN_ZERO_VOLUME:  $WARN_ZERO_VOLUME"

    return output_filename
end
