# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using StaticArrays
using LinearAlgebra

"""
    Tet10(nodes, point)

3D tetrahedral cell with 10 `nodes` and `point` coordinates.
"""
struct Tet10 <: Tet
    nodes::Vector{Int}
    # FIXME: should use SubArray but adaptive refinement needs local coordinates
    # point::SubArray{Point{3}}
    point::AbstractArray{Point{3}, 1}
end

# ------------------------------------------------------------------------------
# Properties

# Geometric constants for Tet10.
n_nodes(::Type{Tet10}) = 10
n_sides(::Type{Tet10}) = 4
n_edges(::Type{Tet10}) = 6
n_children(::Type{Tet10}) = 8
n_nodes_per_side(::Type{Tet10}) = 6
n_nodes_per_edge(::Type{Tet10}) = 3

reference_coordinates(::Type{Tet10}) = [
    [0.0, 0.0, 0.0], [1.0, 0.0, 0.0], [0.0, 1.0, 0.0], [0.0, 0.0, 1.0],
    [0.5, 0.0, 0.0], [0.5, 0.5, 0.0], [0.0, 0.5, 0.0],
    [0.0, 0.0, 0.5], [0.5, 0.0, 0.5], [0.0, 0.5, 0.5],
]

# ------------------------------------------------------------------------------
# Functions

function volume(cell::Tet10)
    # See: libmesh/src/geom/cell_tet10.C

    # Make copies of our points.  It makes the subsequent calculations a bit
    # shorter and avoids dereferencing the same pointer multiple times.
    point = cell.point
    x0 = point[1]; x1 = point[2]; x2 = point[3]; x3 = point[4]; x4 = point[5]
    x5 = point[6]; x6 = point[7]; x7 = point[8]; x8 = point[9]; x9 = point[10]

    # The constant components of the dx/dr vector, linear in r, s, t.
    # These were copied directly from the output of a Python script.
    dx_dr =
        [
            -3*x0 - x1 + 4*x4,         # constant
            4*x0 - 4*x4 - 4*x7 + 4*x8, # t
            4*x0 - 4*x4 + 4*x5 - 4*x6, # s
            4*x0 + 4*x1 - 8*x4         # r
        ]

    # The constant components of the dx/ds vector, linear in r, s, t.
    # These were copied directly from the output of a Python script.
    dx_ds =
        [
            -3*x0 - x2 + 4*x6,         # constant
            4*x0 - 4*x6 - 4*x7 + 4*x9, # t
            4*x0 + 4*x2 - 8*x6,        # s
            4*x0 - 4*x4 + 4*x5 - 4*x6  # r
        ]

    # The constant components of the dx/dt vector, linear in r, s, t.
    # These were copied directly from the output of a Python script.
    dx_dt =
        [
            -3*x0 - x3 + 4*x7,         # constant
            4*x0 + 4*x3 - 8*x7,        # t
            4*x0 - 4*x6 - 4*x7 + 4*x9, # s
            4*x0 - 4*x4 - 4*x7 + 4*x8  # r
        ]

    # 2x2x2 conical quadrature rule.  Note: there is also a five point
    # rule for tets with a negative weight which would be cheaper, but
    # we'll use this one to preclude any possible issues with
    # cancellation error.
    N = 8
    w = [3.6979856358852914509238091810505e-02,
         1.6027040598476613723156741868689e-02,
         2.1157006454524061178256145400082e-02,
         9.1694299214797439226823542540576e-03,
         3.6979856358852914509238091810505e-02,
         1.6027040598476613723156741868689e-02,
         2.1157006454524061178256145400082e-02,
         9.1694299214797439226823542540576e-03]

    r = [1.2251482265544137786674043037115e-01,
         5.4415184401122528879992623629551e-01,
         1.2251482265544137786674043037115e-01,
         5.4415184401122528879992623629551e-01,
         1.2251482265544137786674043037115e-01,
         5.4415184401122528879992623629551e-01,
         1.2251482265544137786674043037115e-01,
         5.4415184401122528879992623629551e-01]

    s = [1.3605497680284601717109468420738e-01,
         7.0679724159396903069267439165167e-02,
         5.6593316507280088053551297149570e-01,
         2.9399880063162286589079157179842e-01,
         1.3605497680284601717109468420738e-01,
         7.0679724159396903069267439165167e-02,
         5.6593316507280088053551297149570e-01,
         2.9399880063162286589079157179842e-01]

    t = [1.5668263733681830907933725249176e-01,
         8.1395667014670255076709592007207e-02,
         6.5838687060044409936029672711329e-02,
         3.4202793236766414300604458388142e-02,
         5.8474756320489429588282763292971e-01,
         3.0377276481470755305409673253211e-01,
         2.4571332521171333166171692542182e-01,
         1.2764656212038543100867773351792e-01]

    vol = 0.
    for q = 1:N
        # Compute dx_dr, dx_ds, dx_dt at the current quadrature point.
        dx_dr_q = dx_dr[1] + t[q]*dx_dr[2] + s[q]*dx_dr[3] + r[q]*dx_dr[4]
        dx_ds_q = dx_ds[1] + t[q]*dx_ds[2] + s[q]*dx_ds[3] + r[q]*dx_ds[4]
        dx_dt_q = dx_dt[1] + t[q]*dx_dt[2] + s[q]*dx_dt[3] + r[q]*dx_dt[4]

        # Compute scalar triple product, multiply by weight, and accumulate volume.
        vol += w[q] * triple_scalar_product(dx_dr_q, dx_ds_q, dx_dt_q)
    end

    return vol
end
