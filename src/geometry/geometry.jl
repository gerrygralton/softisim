# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using StaticArrays
import PyPlot

# ------------------------------------------------------------------------------
# Type aliases for points

"""
    Point{Dim}

A geometric point in `Dim`-dimensional space.
"""
Point{Dim} = SVector{Dim, Float64}

"""
    Point(a, b)

Construct a 1-dimensional point with coordinates (`a`).
"""
Point(a) = Point{1}(a)

"""
    Point(a, b)

Construct a 2-dimensional point with coordinates (`a`, `b`).
"""
Point(a, b) = Point{2}(a, b)

"""
    Point(a, b, c)

Construct a 3-dimensional point with coordinates (`a`, `b`, `c`).
"""
Point(a, b, c) = Point{3}(a, b, c)

# ------------------------------------------------------------------------------

"""
Abstract supertype for all geometric elements.

A 1D `Elem` is an `Edge`,
a 2D `Elem` is a `Face`,
a 3D `Elem` is a `Cell`,
etc.
"""
abstract type Elem{Dim} end

# ------------------------------------------------------------------------------

"Abstract supertype for all 1D geometric elements."
abstract type Edge <: Elem{1} end

"Abstract supertype for all 2D geometric elements."
abstract type Face <: Elem{2} end

"Abstract supertype for all 3D geometric elements."
abstract type Cell <: Elem{3} end

# ------------------------------------------------------------------------------

"Abstract supertype for all tetrahedral geometric elements."
abstract type Tet <: Cell end

"Abstract supertype for all hexahedral geometric elements."
abstract type Hex <: Cell end

# ------------------------------------------------------------------------------
# Properties

"""
    n_dims(type::Type{Point}) -> Integer
    n_dims(point::Point) -> Integer

Return the number of spatial dimensions of this `point`
or of a point of this `type`.
"""
n_dims(::Union{<:Point{Dim}, Type{<:Point{Dim}}}) where Dim = Dim

"""
    n_dims(type::Type{Elem}) -> Integer
    n_dims(element::Elem) -> Integer

Return the number of spatial dimensions of this `element`
or of an element of this `type`.
"""
n_dims(::Union{<:Elem{Dim}, Type{<:Elem{Dim}}}) where Dim = Dim

"""
    n_nodes(type::Type{Elem}) -> Integer
    n_nodes(element::Elem) -> Integer

Return the number of nodes this `element` or an element of this `type` contains.
"""
n_nodes(type::Type{<:Elem})
n_nodes(elem::Elem) = n_nodes(typeof(elem))

"""
    n_sides(type::Type{<:Elem}) -> Integer
    n_sides(element::Elem) -> Integer

Return the number of sides this `element` or an element of this `type` contains.
"""
n_sides(type::Type{<:Elem})
n_sides(elem::Elem) = n_sides(typeof(elem))

"""
    n_edges(type::Type{<:Elem}) -> Integer
    n_edges(element::Elem) -> Integer

Return the number of edges this `element` or an element of this `type` contains.
"""
n_edges(type::Type{<:Elem})
n_edges(elem::Elem) = n_edges(typeof(elem))

"""
    n_children(type::Type{<:Elem}) -> Integer
    n_children(element::Elem) -> Integer

Return the number of children this `element` or an element of this `type` may have.
"""
n_children(type::Type{<:Elem})
n_children(elem::Elem) = n_children(typeof(elem))

"""
    n_nodes_per_side(type::Type{<:Elem}) -> Integer
    n_nodes_per_side(element::Elem) -> Integer

Return the number of nodes per side this `element` or an element of this `type` contains.
"""
n_nodes_per_side(type::Type{<:Elem})
n_nodes_per_side(elem::Elem) = n_nodes_per_side(typeof(elem))

"""
    n_nodes_per_edge(type::Type{<:Elem}) -> Integer
    n_nodes_per_edge(element::Elem) -> Integer

Return the number of nodes per edge this `element` or an element of this `type` contains.
"""
n_nodes_per_edge(type::Type{<:Elem})
n_nodes_per_edge(elem::Elem) = n_nodes_per_edge(typeof(elem))

"""
    reference_coordinates(type::Type{<:Elem})
    reference_coordinates(element::Elem)

Return the reference coordinates
for an element of the given `type` or
for an element of the same type as the given `element`.
"""
reference_coordinates(type::Type{<:Elem})
reference_coordinates(elem::Elem) = reference_coordinates(typeof(elem))

"""
    reference_coordinates_as_matrix(type::Type{<:Elem}) -> Matrix{Float64}
    reference_coordinates_as_matrix(element::Elem) -> Matrix{Float64}

Return the reference coordinates
for an element of the given `type` or
for an element of the same type as the given `element`
in the form of a matrix.
"""
reference_coordinates_as_matrix(
    elem::Union{<:Elem{Dim}, Type{<:Elem{Dim}}}) where {Dim} =
        hcat(reference_coordinates(elem)...)

"""
    reference_coordinates_as_points(type::Type{<:Elem}) -> Vector{Point}
    reference_coordinates_as_points(element::Elem) -> Vector{Point}

Return the reference coordinates
for an element of the given `type` or
for an element of the same type as the given `element`
in the form of a `Vector` of `Point`s.
"""
reference_coordinates_as_points(type::Type{<:Elem{Dim}}) where {Dim} =
    [Point{Dim}(point) for point in reference_coordinates(type)]
reference_coordinates_as_points(elem::Elem) =
    reference_coordinates_as_points(typeof(elem))

"""
    reference_element(type::Type{<:Elem}) -> Elem
    reference_element(element::Elem) -> Elem

Return a reference element
of the given `type` or
of the same type as the given `element`.
"""
reference_element(type::Type{<:Elem}) = type(1:n_nodes(type), reference_coordinates_as_points(type))
reference_element(elem::Elem) = reference_element(typeof(elem))

# ------------------------------------------------------------------------------
# Constants for all reference elements

"""
    reference_volume(type::Type{<:Elem}) -> Elem
    reference_volume(element::Elem) -> Elem

Return the volume of a reference element
of the given `type` or
of the same type as the given `element`.
"""
reference_volume(elem::Union{Elem, Type{<:Elem}})

# 3D
reference_volume(::Union{Hex, Type{<:Hex}}) = Float64(8)
reference_volume(::Union{Tet, Type{<:Tet}}) = Float64(1/6)

# ------------------------------------------------------------------------------
# Functions

"""
    plot_element(element)

Plot the given `element`.

# Examples

```jldoctest
julia> F = [-2 -1 0; 3/2 -1/2 0; 0 0 1]
3×3 Array{Float64,2}:
 -2.0  -1.0  0.0
  1.5  -0.5  0.0
  0.0   0.0  1.0

julia> x = reference_coordinates_as_matrix(Hex8)
3×8 Array{Float64,2}:
 -1.0   1.0   1.0  -1.0  -1.0   1.0  1.0  -1.0
 -1.0  -1.0   1.0   1.0  -1.0  -1.0  1.0   1.0
 -1.0  -1.0  -1.0  -1.0   1.0   1.0  1.0   1.0

julia> y = F*x
3×8 Array{Float64,2}:
  3.0  -1.0  -3.0   1.0   3.0  -1.0  -3.0   1.0
 -1.0   2.0   1.0  -2.0  -1.0   2.0   1.0  -2.0
 -1.0  -1.0  -1.0  -1.0   1.0   1.0   1.0   1.0

julia> dx = reference_element(Hex8)
Hex8([1, 2, 3, 4, 5, 6, 7, 8], StaticArrays.SArray{Tuple{3},Float64,1,3}[[-1.0, -1.0, -1.0], [1.0, -1.0, -1.0], [1.0, 1.0, -1.0], [-1.0, 1.0, -1.0], [-1.0, -1.0, 1.0], [1.0, -1.0, 1.0], [1.0, 1.0, 1.0], [-1.0, 1.0, 1.0]])

julia> dy = Hex8(1:8, y)
Hex8([1, 2, 3, 4, 5, 6, 7, 8], StaticArrays.SArray{Tuple{3},Float64,1,3}[[3.0, -1.0, -1.0], [-1.0, 2.0, -1.0], [-3.0, 1.0, -1.0], [1.0, -2.0, -1.0], [3.0, -1.0, 1.0], [-1.0, 2.0, 1.0], [-3.0, 1.0, 1.0], [1.0, -2.0, 1.0]])

julia> plot_element(dx);

julia> plot_element(dy);

```
"""
function plot_element(elem::Cell)
    p = elem.point
    xs = getindex.(p, 1)
    ys = getindex.(p, 2)
    zs = getindex.(p, 3)
    PyPlot.scatter3D(xs, ys, zs)
    for (n, x, y, z) in zip(elem.nodes, xs, ys, zs)
        PyPlot.text3D(x, y, z, string(n))
    end
    PyPlot.xlabel("x")
    PyPlot.ylabel("y")
    PyPlot.zlabel("z")
end

# TODO: add plot_element functions for 1D and 2D elements.

"""
    volume(cell::Cell)

Compute the volume of this `cell`.
"""
volume(cell::Cell)

# ------------------------------------------------------------------------------
# Concrete types (each defined in its own file)

# 1D (Edges)

# 2D (Faces)

# 3D (Cells)
include("cell_hex8.jl")
include("cell_hex20.jl")
include("cell_tet4.jl")
include("cell_tet10.jl")

# ------------------------------------------------------------------------------
# Generated code for all concrete types

# Constructors that take a matrix or a vector of vectors
# (instead of vector of points) as the coordinates.
# TODO: add documentation
# TODO: add similar functions for 1D and 2D elements
for type in (:Hex8, :Hex20, :Tet4, :Tet10)
    @eval begin
        function $type(nodes, coord::Matrix{Float64})
            nd = size(coord, 1)
            nn = size(coord, 2)
            @assert nd == n_dims($type)
            @assert nn == n_nodes($type)
            return $type(nodes, [Point{nd}(coord[:,i]) for i = 1:nn])
        end

        function $type(nodes, coord::Vector{Vector{Float64}})
            nd = length(coord[1]) # dim of the first point
            nn = length(coord)
            @assert nd == n_dims($type)
            @assert nn == n_nodes($type)
            return $type(nodes, [Point{nd}(coord[i]) for i = 1:nn])
        end
    end
end
