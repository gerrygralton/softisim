# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using LinearAlgebra
using LinearAlgebra.BLAS

"""
    InterpMLS_3D_2(glob, xi, R_node, exact_derivatives)
"""
function InterpMLS_3D_2(
    centers::Vector{Point{3}},
    support::Vector{Float64},
    xi::Point{3},
    exact_derivatives::Bool,
    basic_function::BasicFunction
)::Tuple{Vector{Int}, Vector{Float64}, Matrix{Float64}}

    coord = eltorows(centers)

    xi1 = xi[1]
    xi2 = xi[2]
    xi3 = xi[3]

    # Compute distance between nodes and the selected point
    dist = zeros(size(coord, 1))
    for i = 1:size(coord, 1)
        dist[i] = sqrt((coord[i,1] - xi1)^2 +
                       (coord[i,2] - xi2)^2 +
                       (coord[i,3] - xi3)^2)
    end

    # Find the support nodes which influence this point and their distances
    # FIXME: this should be moved and improved
    if (hasproperty(basic_function, :num_support_nodes) &&
        basic_function.num_support_nodes > 0)
        # Define neighbors by k nearest
        sort_dist = sort(dist)
        R_node = sort_dist[basic_function.num_support_nodes]
        closest_nodes = findall(dist .<= R_node)
        # Restrict to k? Probably not---might be unpredictable
        # closest_nodes = findall(dist .<= R_node)[1:basic_function.num_support_nodes]
    else
        # Define neighbors by distance from support radius
        closest_nodes = findall(dist .< support)
        R_node = @view support[closest_nodes]
    end

    # Compute MLS shape functions's derivatives
    # Assemble the matrices
    X = @view coord[closest_nodes,1];
    Y = @view coord[closest_nodes,2];
    Z = @view coord[closest_nodes,3];
    # find weights: quartic spline weight funciton is used (Meshfree book P72)
    Di = @. sqrt.((X-xi1).*(X-xi1) + (Y-xi2).*(Y-xi2) + (Z-xi3).*(Z-xi3))
    # "R_node" is the support radius for each and defines the domain where Wi!=0
    ri = Di./R_node;
    Wi = basic_value(basic_function, ri)

    # Allocate arrays
    # "m" is the number of polynomial; "n" is the number of support nodes
    m = 10
    n = length(closest_nodes)
    A = zeros(m, m)
    B = zeros(m, n)
    pxi = zeros(m)
    # if exact_derivatives
    dA_dx = zeros(m, m)
    dA_dy = zeros(m, m)
    dA_dz = zeros(m, m)
    dB_dx = zeros(m, n)
    dB_dy = zeros(m, n)
    dB_dz = zeros(m, n)

    sfval = zeros(n)
    sfd = zeros(n, 3)

    # compute weighted moment matrix and B matrix
    for i = 1:n
        pxi[1] = 1.
        pxi[2] = X[i]
        pxi[3] = Y[i]
        pxi[4] = Z[i]
        pxi[5] = X[i]^2
        pxi[6] = Y[i]^2
        pxi[7] = Z[i]^2
        pxi[8] = X[i]*Y[i]
        pxi[9] = Y[i]*Z[i]
        pxi[10] = X[i]*Z[i]
        # A .+= Wi[i] * pxi * pxi'
        gemm!('N', 'T', Wi[i], pxi, pxi, 1., A)
        # B[:,i] = Wi[i] * pxi
        for j = 1:m
            B[j,i] = Wi[i] * pxi[j]
        end
    end

    # MMLS parameter (for classical MLS set cf = 0)
    cf = 1e-7
    A[5,5] += cf
    A[6,6] += cf
    A[7,7] += cf
    A[8,8] += cf
    A[9,9] += cf
    A[10,10] += cf

    factA = factorize(A)
    coefT = factA\B;
    if exact_derivatives != 0   # compute exact shape functions derivatives
        # compute derivative of weight function
        dWi_dri_div_ri = basic_dwdr_div_r(basic_function, ri) # (dWi/dri)/ri
        dri_dx_mul_ri = @. -(X-xi1)./(R_node.*R_node); # (dri/dx)*ri
        dri_dy_mul_ri = @. -(Y-xi2)./(R_node.*R_node); # (dri/dy)*ri
        dri_dz_mul_ri = @. -(Z-xi3)./(R_node.*R_node); # (dri/dz)*ri
        dWi_dx = (dWi_dri_div_ri).*dri_dx_mul_ri; # dwi/dx = (dwi/dri)*(dri/dx)
        dWi_dy = (dWi_dri_div_ri).*dri_dy_mul_ri; # dwi/dy
        dWi_dz = (dWi_dri_div_ri).*dri_dz_mul_ri; # dwi/dz
        # compute derivatives of A and B matrices
        for i = 1:n
            pxi[1] = 1.
            pxi[2] = X[i]
            pxi[3] = Y[i]
            pxi[4] = Z[i]
            pxi[5]  = X[i]^2
            pxi[6]  = Y[i]^2
            pxi[7]  = Z[i]^2
            pxi[8]  = X[i]*Y[i]
            pxi[9]  = Y[i]*Z[i]
            pxi[10] = X[i]*Z[i]
            # dA_d_ .+= dWi_d_[i] * pxi * pxi'
            gemm!('N', 'T', dWi_dx[i], pxi, pxi, 1., dA_dx)
            gemm!('N', 'T', dWi_dy[i], pxi, pxi, 1., dA_dy)
            gemm!('N', 'T', dWi_dz[i], pxi, pxi, 1., dA_dz)
            # dB_d_[:,i] = dWi_d_[i] * pxi
            for j = 1:m
                dB_dx[j,i] = dWi_dx[i] * pxi[j]
                dB_dy[j,i] = dWi_dy[i] * pxi[j]
                dB_dz[j,i] = dWi_dz[i] * pxi[j]
            end
        end
        # compute sfd
        B1x = dB_dx .- dA_dx*coefT;
        B1y = dB_dy .- dA_dy*coefT;
        B1z = dB_dz .- dA_dz*coefT;
        coefT1 = [factA\B1x, factA\B1y, factA\B1z]

        sfd .= 0
        @fastmath @inbounds @simd for j = 1:n
            sfd[j,1] += coefT[2,j]
            sfd[j,1] += 2*xi1*coefT[5,j]
            sfd[j,1] += xi2*coefT[8,j]
            sfd[j,1] += xi3*coefT[10,j]
            sfd[j,1] += coefT1[1][1,j]
            sfd[j,1] += coefT1[1][2,j]*xi1
            sfd[j,1] += coefT1[1][3,j]*xi2
            sfd[j,1] += coefT1[1][4,j]*xi3
            sfd[j,1] += coefT1[1][5,j]*xi1^2
            sfd[j,1] += coefT1[1][6,j]*xi2^2
            sfd[j,1] += coefT1[1][7,j]*xi3^2
            sfd[j,1] += coefT1[1][8,j]*xi1*xi2
            sfd[j,1] += coefT1[1][9,j]*xi2*xi3
            sfd[j,1] += coefT1[1][10,j]*xi1*xi3

            sfd[j,2] += coefT[3,j]
            sfd[j,2] += 2*xi2*coefT[6,j]
            sfd[j,2] += xi1*coefT[8,j]
            sfd[j,2] += xi3*coefT[9,j]
            sfd[j,2] += coefT1[2][1,j]
            sfd[j,2] += coefT1[2][2,j]*xi1
            sfd[j,2] += coefT1[2][3,j]*xi2
            sfd[j,2] += coefT1[2][4,j]*xi3
            sfd[j,2] += coefT1[2][5,j]*xi1^2
            sfd[j,2] += coefT1[2][6,j]*xi2^2
            sfd[j,2] += coefT1[2][7,j]*xi3^2
            sfd[j,2] += coefT1[2][8,j]*xi1*xi2
            sfd[j,2] += coefT1[2][9,j]*xi2*xi3
            sfd[j,2] += coefT1[2][10,j]*xi1*xi3

            sfd[j,3] += coefT[4,j]
            sfd[j,3] += 2*xi3*coefT[7,j]
            sfd[j,3] += xi2*coefT[9,j]
            sfd[j,3] += xi1*coefT[10,j]
            sfd[j,3] += coefT1[3][1,j]
            sfd[j,3] += coefT1[3][2,j]*xi1
            sfd[j,3] += coefT1[3][3,j]*xi2
            sfd[j,3] += coefT1[3][4,j]*xi3
            sfd[j,3] += coefT1[3][5,j]*xi1^2
            sfd[j,3] += coefT1[3][6,j]*xi2^2
            sfd[j,3] += coefT1[3][7,j]*xi3^2
            sfd[j,3] += coefT1[3][8,j]*xi1*xi2
            sfd[j,3] += coefT1[3][9,j]*xi2*xi3
            sfd[j,3] += coefT1[3][10,j]*xi1*xi3
        end
    else   # compute diffuse shape functions derivatives
        @error "FIXME!"
        # sfd_x = coefT[2,j] + 2*xi1*coefT[5,j] + xi2*coefT[8,j] + xi3*coefT[10,j];
        # sfd_y = coefT[3,j] + 2*xi2*coefT[6,j] + xi1*coefT[8,j] + xi3*coefT[9,j];
        # sfd_z = coefT[4,j] + 2*xi3*coefT[7,j] + xi2*coefT[9,j] + xi1*coefT[10,j];
    end

    # compute shape functions values
    sfval .= 0
    @fastmath @inbounds @simd for j = 1:n
        sfval[j] += coefT[1,j]
        sfval[j] += coefT[2,j]*xi1
        sfval[j] += coefT[3,j]*xi2
        sfval[j] += coefT[4,j]*xi3
        sfval[j] += coefT[5,j]*xi1^2
        sfval[j] += coefT[6,j]*xi2^2
        sfval[j] += coefT[7,j]*xi3^2
        sfval[j] += coefT[8,j]*xi1*xi2
        sfval[j] += coefT[9,j]*xi2*xi3
        sfval[j] += coefT[10,j]*xi1*xi3
    end

    return closest_nodes, sfval, sfd
end
