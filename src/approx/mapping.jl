# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

"""
    map_from_reference_cell(interp, cell, xref)

Map the point `xref` from the reference cell to coordinates `x` in `cell`
using `interp`olation.
"""
function map_from_reference_cell(interp::FELagrange, cell::Cell, xref::Point{Dim}) where {Dim}
    N = shape_value(interp, cell, xref)
    x = zeros(Dim)
    for (i, pi) in enumerate(cell.point)
        for j = 1:Dim
            x[j] += N[i] * pi[j]
        end
    end
    return Point{Dim}(x)
end

# Default mappings
# FIXME: remove these and always pass interp to map_from_reference_cell
"""
    map_from_reference_cell(cell, xref)

Map the point `xref` from the reference cell to coordinates `x` in `cell`
using default interpolation.
"""
map_from_reference_cell(cell::Union{Tet4, Hex8}, xref::Point{Dim}) where {Dim} =
    map_from_reference_cell(FELagrange(1), cell, xref)
map_from_reference_cell(cell::Union{Tet10, Hex20}, xref::Point{Dim}) where {Dim} =
    map_from_reference_cell(FELagrange(2), cell, xref)
