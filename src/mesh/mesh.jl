using DataStructures: OrderedDict

struct Mesh{Dim}
    node_ids::Vector{Int}
    nodes::Vector{Int}                       # TODO: deprecate this field (equivalent to above)

    node_coordinates::Vector{Point{Dim}}
    coordinates::Vector{Point{Dim}}          # TODO: deprecate this field (equivalent to above)

    node_sets::OrderedDict{String, Vector{Int}}
    nsets::OrderedDict{String, Vector{Int}}  # TODO: deprecate this field (equivalent to above)

    element_ids::Vector{Int}

    element_types::Vector{Symbol}

    element_connectivity::Vector{Vector{Int}}
    elements::Matrix{Int}                    # TODO: deprecate this field (similar to above)

    # TODO: Move elemets/cells from `create_elements` function to here

    element_sets::OrderedDict{String, Vector{Int}}
    elsets::OrderedDict{String, Vector{Int}} # TODO: deprecate this field (equivalent to above)

    n_nodes::Int                             # TODO: deprecate and make this a method instead

    dim::Int                                 # TODO: deprecate this field (equivalent to Dim above)

    """
        Mesh{Dim}(node_coordinates; ...)

    Create a new mesh of spatial dimensions `Dim` with given
    `node_coordinates` and (optionally) `element_connectivity`.

    Only the coordinates of the nodal points are required,
    in which case the mesh is a point cloud
    (connectivity may be added later by triangulation).
    """
    function Mesh{Dim}(
        node_coordinates     ::Union{Vector{Point{Dim}}, Matrix{Float64}};
        # Optional arguements"
        node_ids             ::Union{Nothing, Vector{Int}} = nothing,
        node_sets            ::Union{Nothing, OrderedDict{String, Vector{Int}}} = nothing,
        element_connectivity ::Union{Nothing, Vector{Vector{Int}}, Matrix{Int}} = nothing,
        element_types        ::Union{Nothing, Vector{Symbol}} = nothing,
        element_ids          ::Union{Nothing, Vector{Int}} = nothing,
        element_sets         ::Union{Nothing, OrderedDict{String, Vector{Int}}} = nothing,
    ) where Dim

        # Convert node_coordinates from Matrix to Vector of Points
        if node_coordinates isa Matrix{Float64}
            node_coordinates = [Point{Dim}(node_coordinates[i,:] for i in size(node_coordinates, 1))]
        end

        # Create node_ids if not given (1, ..., nn)
        if node_ids isa Nothing
            node_ids = collect(1:length(node_coordinates))
        end

        # Create empty node_sets if not given
        if node_sets isa Nothing
            node_sets = OrderedDict{String, Vector{Int}}()
        end

        # Element connectivity is optional
        if !(element_connectivity isa Nothing)
            # Convert element_connectivity Matrix to Vector{Vector{Int}}
            # TODO: keep `elements` for now; some code depends on this matrix form
            if element_connectivity isa Matrix{Int}
                elements = copy(element_connectivity)
                element_connectivity = Vector{Int}[]
                for i = 1:size(elements, 1)
                    push!(element_connectivity, elements[i,:])
                end
            end

            # Guess element_types if not given
            if element_types isa Nothing
                element_types = Symbol[]
                for elem in element_connectivity
                    elem_type = Dict(
                        4  => :Tet4,
                        8  => :Hex8,
                        10 => :Tet10,
                        20 => :Hex20,
                    )[length(elem)]
                    push!(element_types, elem_type)
                end
            end

            # Create element_ids if not given (1, ..., ne)
            if element_ids isa Nothing
                element_ids = collect(1:length(element_connectivity))
            end

            # Create empty element_sets if not given
            if element_sets isa Nothing
                element_sets = OrderedDict{String, Vector{Int}}()
            end
        end

        return new{Dim}(
            node_ids::Vector{Int},
            node_ids::Vector{Int},# TODO: deprecate

            node_coordinates::Vector{Point{Dim}},
            node_coordinates::Vector{Point{Dim}},# TODO: deprecate

            node_sets::OrderedDict{String, Vector{Int}},
            node_sets::OrderedDict{String, Vector{Int}},# TODO: deprecate

            element_ids::Vector{Int},

            element_types::Vector{Symbol},

            element_connectivity::Vector{Vector{Int}},
            eltorows(element_connectivity)::Matrix{Int},# TODO: deprecate

            element_sets::OrderedDict{String, Vector{Int}},
            element_sets::OrderedDict{String, Vector{Int}},# TODO: deprecate

            length(node_coordinates)::Int,

            Dim::Int
        )
    end
end

"""
Redefine element connectivity using triangulation of VERTICES.
This would not work for higher order (e.g. quadratic) mesh.
"""
# FIXME: not implemented
function triangulate!(mesh::Mesh)
    mesh.element_connectivity = trianglulate(mesh.node_coordinates)
end
