# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

# Prototypes for user-defined functions
#
# These functions can be redefined by the user in the input file.
#
# NOTE: The function argument types must be the same as specified in this file
#       otherwise the existing method defined here will not be overwritten
#       but a new method will be defined instead!

abstract type AbstractUserVariables end

"""
    user_pre_solve!(...)

This function is called before the solve function.
"""
function user_pre_solve!(
    user::Union{Nothing, AbstractUserVariables},
    glob::GlobalVariables
)::Nothing

    @debug "user_pre_solve!: nothing"

    return
end

"""
    user_post_solve!(...)

This function is called after the solve function.
"""
function user_post_solve!(
    user::Union{Nothing, AbstractUserVariables},
    glob::GlobalVariables
)::Nothing

    @debug "user_post_solve!: nothing"

    return
end

"""
    user_bc!(...)

Apply user defined essential BCs.

This function is called within the solver timestepping loop
after ordinary essential BCs have been applied.
"""
function user_bc!(
    user::Union{Nothing, AbstractUserVariables},
    glob::GlobalVariables,
    prm,
    time_step_size::Float64
)::Nothing

    @debug "user_bc!: nothing"

    return
end
