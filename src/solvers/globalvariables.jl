# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using SparseArrays

mutable struct GlobalVariables{Dim}

    # Numbers
    increment::Int
    time::Float64

    total_mass_nodes::Float64
    total_mass_qpoints::Float64
    total_mass_qpoints_unscaled::Float64
    total_strain_energy::Float64

    qpoints::QuadraturePoint{Dim}

    # FIXME: this should probably be defined only inside mesh
    # but some functions use glob to access coordinates.
    coord::Vector{Point{Dim}}

    # Solution variable arrays
    disp::Matrix{Float64}
    dnod_curr::Matrix{Float64}
    dnod_next::Matrix{Float64}
    dnod_prev::Matrix{Float64}
    forc_tot::Matrix{Float64}
    forc_int::Matrix{Float64}
    forc_ebc::Matrix{Float64}
    mass::AbstractMatrix{Float64}

    # Basis function values
    phi::SparseMatrixCSC{Float64,Int64}

    support_radius::Vector{Float64}

    # EBCIEM
    ebc_nodes::Vector{Vector{Int}}
    ebc_transforms::Vector{SparseMatrixCSC{Float64, Int}}

    function GlobalVariables{Dim}(coord) where Dim
        # Incomplete initialisation

        glob = new{Dim}()

        glob.coord = coord

        glob.qpoints = QuadraturePoint{Dim}()

        zero!(glob)

        return glob
    end
end

function zero!(glob::GlobalVariables{Dim}) where Dim

    glob.increment = 0
    glob.time = 0.

    # Number of nodes
    n = n_nodes(glob)

    # Preallocate arrays
    glob.disp      = zeros(Float64, n, Dim)
    glob.dnod_curr = zeros(Float64, n, Dim)
    glob.dnod_next = zeros(Float64, n, Dim)
    glob.dnod_prev = zeros(Float64, n, Dim)
    glob.forc_tot  = zeros(Float64, n, Dim)
    glob.forc_int  = zeros(Float64, n, Dim)
    glob.forc_ebc  = zeros(Float64, n, Dim)
end

# ------------------------------------------------------------------------------

dim(::GlobalVariables{Dim}) where Dim = Dim
n_nodes(glob::GlobalVariables) = length(glob.coord)
