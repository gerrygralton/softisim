# SofTiSim (not only) the Soft Tissue Simulator
# Copyright (C) 2014-2020 Benjamin Zwick
# SofTiSim is free software: See COPYING for details
# SPDX-License-Identifier: GPLv3-or-later

using Profile, ProfileView
using SofTiSim: zero!

# Reset global variables
zero!(glob)

# Do not output again while profiling (otherwise errors may occur)
output_field = nothing
output_history = nothing

Profile.init(n=Int(1e8))
Profile.clear()

# ------------------------------------------------------------------------------
printstyled("@profile solve\n", color=:magenta)

@profile results = solve!(
    glob,
    uservars,
    toc,
    time_step_size,
    stepnum_total,
    model[:progress_num_steps],
    stepnum_load,
    model[:use_EBCIEM],
    bcs,
    output_field,
    output_history,
    solver)

# Profile.print(maxdepth=9)

ProfileView.view()
