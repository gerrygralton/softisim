# Contributing to SofTiSim

Make everybody feel welcome (including new students and old professors).

## Documentation

Julia enables package developers and users to document functions,
types and other objects easily via a built-in
[documentation](https://docs.julialang.org/en/latest/manual/documentation/index.html)

## Style

Indentation: 4 spaces.

Please follow the Julia [style guide](https://docs.julialang.org/en/latest/manual/style-guide/index.html)

Some things to note:

1. Write reusable code:
   a. Write small functions that do one thing many times.
   b. Write scripts that do one thing once.

2. Do not copy and paste code.
   If you need to use the same code in more than one place
   either create a function to replace the original code,
   or split the original file into two separate files and
   include as required.

## Using Git

1. Do not upload large mesh or data files
   (the repository will grow beyond control)

## Comments

Please use the following conventions for comments in code
(in order of severity)

| `FIXME:` | A bug that MUST be fixed                                                                          |
| `TODO:`  | Something that could be improved or checked                                                       |
| `NOTE:`  | Useful information regarding a section of code that is not directly related to the line below it. |

## Structure of the code

- Functions should be defined in the SofTiSim module namespace (not Main).

- Do NOT operate on global variables inside functions
  (use the module namespace to separate from Main).
  However global variables can be very useful in Main and scripts.
