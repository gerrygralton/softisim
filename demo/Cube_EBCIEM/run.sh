julia --color=yes --check-bounds=no -O3 Cube_EBCIEM_neohookean.jl --overwrite=true
julia --color=yes --check-bounds=no -O3 Cube_EBCIEM_ogden.jl --overwrite=true
