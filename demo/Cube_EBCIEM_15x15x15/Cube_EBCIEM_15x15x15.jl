include("../init.jl")

# Meshless Simulation : Unconstrained Deformation of a Cube
# EBC Impostion : Using 'EBCIEM/SEBCIEM' or 'Correction' Method

model = Dict()

#------------------------------------------------------------------------------
# Configuration for mesh-free methods

# Shape functions
model[:use_exact_SF_derivatives] = true
model[:use_base_functions] = 2
model[:support_radius_dilatation] = 1.6
model[:use_variable_support_radius] = true

# Essential Boundary Condition Impostion
model[:use_EBCIEM] = true
model[:use_Simplified_EBCIEM] = false

# Adaptive numerical integration
model[:use_adaptive_integration] = false
model[:number_of_tetrahedral_divisions] = 4
model[:integration_eps] = 0.1
model[:quadrature_degree] = 1

model[:scale_mass] = true

#------------------------------------------------------------------------------
# Other configurations

model[:progress_num_steps] = 500

#------------------------------------------------------------------------------
# Solver

using SofTiSim: DynamicRelaxationParameters

solver = DynamicRelaxationParameters(
    configured_time_step = 0.0005,
)

#------------------------------------------------------------------------------
# Materials

using SofTiSim: NeoHookean, elastic_moduli

model[:density] = 1000
model[:material] = NeoHookean(elastic_moduli(E=3000, nu=0.49))

#------------------------------------------------------------------------------
# Mesh

using SofTiSim: readmesh

mesh = readmesh("../mesh/cube_15x15x15_C3D4H_mesh.inp")

#------------------------------------------------------------------------------
# Dirichlet boundary conditions

using SofTiSim: DirichletBC, smooth345

bcs = (
    dx_0 = DirichletBC(
        nodes = mesh.nsets["DX_0"],
        dofs = 1:1,
    ),
    dy_0 = DirichletBC(
        nodes = mesh.nsets["DY_0"],
        dofs = 2:2,
    ),
    dz_0 = DirichletBC(
        nodes = mesh.nsets["DZ_0"],
        dofs = 3:3,
    ),
    disp = DirichletBC(
        nodes = mesh.nsets["DISPLACED"],
        dofs = 3:3,
        magnitude = -0.02,
        amplitude = smooth345,
    ),
)

#------------------------------------------------------------------------------
# Output

using SofTiSim: OutputFieldVtk

output_field = OutputFieldVtk(interval=200)

#------------------------------------------------------------------------------
# Solve with MTLED

include("../../src/mtled.jl")

#------------------------------------------------------------------------------
# Postprocess

using SofTiSim: nrmse
using DelimitedFiles: readdlm

uz_exact = -0.2 .* getindex.(glob.coord, 3)

# Compare against Abaqus results
u_abq = readdlm("cube_15x15x15_C3D4H__disp.txt")

@info string("NRMSE Ux MTLED compared to Abaqus: ", nrmse(glob.disp[:,1], u_abq[:,1]))
@info string("NRMSE Uy MTLED compared to Abaqus: ", nrmse(glob.disp[:,2], u_abq[:,2]))
@info string("NRMSE Uz MTLED compared to Abaqus: ", nrmse(glob.disp[:,3], u_abq[:,3]))

@info string("NRMSE Uz Abaqus compared to analytical: ", nrmse(u_abq[:,3], uz_exact))
@info string("NRMSE Uz MTLED compared to analytical: ", nrmse(glob.disp[:,3], uz_exact))
