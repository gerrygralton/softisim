# Julia script for reading and post-processing files saved from ParaView.

# TODO: remove this when SofTiSim is an installable package
push!(LOAD_PATH,"../../src")

job = "cylinder_indentation"
input_filename = job * "/" * job * ".field.csv"
output_filename = "plots/" * job

# -----------------------------------------------------------------------------
# Read data from csv file saved from paraview
import CSV

# The current gold standard (most accurate solution)
# gold = CSV.File("cylinder_indentation.au.csv", delim=",", comment="#")

# Results
# TODO: read actual results from csv file specified on command line
# and compare with the gold standard
results = CSV.File(input_filename, delim=",", comment="#")

# ------------------------------------------------------------------------------
# Results
x1 = results.coord_1;
x2 = results.coord_2;
x3 = results.coord_3;
r = sqrt.(x1.^2 + x2.^2);
u1 = results.disp_1;
u2 = results.disp_2;
u3 = results.disp_3;
ur = sqrt.(u1.^2 + u2.^2);
f_tot_1 = results.forc_tot_1;
f_tot_2 = results.forc_tot_2;
f_tot_3 = results.forc_tot_3;
f_tot_r = sqrt.(f_tot_1.^2 + f_tot_2.^2);
f_tot_m = sqrt.(f_tot_1.^2 + f_tot_2.^2 + f_tot_3.^2);
f_ebc_1 = results.forc_ebc_1;
f_ebc_2 = results.forc_ebc_2;
f_ebc_3 = results.forc_ebc_3;

# ------------------------------------------------------------------------------
# Regions (node set index arrays)

TOL = 1e-6                                  # precision of coordinates
R = maximum(r)                              # radius of cylinder
H = maximum(x3)                             # height of cylinder
top = x3 .> H-TOL;                          # top surface
bot = x3 .< 0+TOL;                          # bottom surface
mid = ((H/2-TOL) .< x3) .& (x3 .< (H/2+TOL)); # middle cross section
# FIXME: include nodes on top that are not displaced
free = .!top .& .!bot

# ------------------------------------------------------------------------------
# Plots

using PyPlot

# ------------------------------------------------------------------------------
# Scatter plots

fig, ax = subplots()
scatter3D(x1[bot], x2[bot], x3[bot], label="bot", c="yellow")
scatter3D(x1[mid], x2[mid], x3[mid], label="mid", c="orange")
scatter3D(x1[top], x2[top], x3[top], label="top", c="red")

# Function to make plotting easier
p(x1, x2, x3, nset, label, color) = scatter3D(x1[nset], x2[nset], x3[nset],
                                              label=label, color=color)

clf()
p(x1, x2, x3, bot, "Bottom", "yellow")
p(x1, x2, x3, mid, "Middle", "orange")
p(x1, x2, x3, top, "Top", "red")
legend()
savefig(output_filename * ".plot.nsets-verify.pdf")

clf()
p(x1, x2, x3, bot, "Bottom", "yellow")
p(x1, x2, x3, top, "Top", "red")
p(x1, x2, x3, free, "Free", "magenta")
legend()
fig.savefig(output_filename * ".plot.nsets-bc.pdf")

# ------------------------------------------------------------------------------
# Histograms

nbins = 10

# Natural boundary
hist(f_tot_3[free], label="Internal force on free nodes", bins=nbins)
savefig(output_filename * ".plot.histogram-force-neumann-boundary.pdf")

# Essential boundary
# Bottom
clf()
hist(f_tot_3[bot], label="Internal force on free nodes", bins=nbins)
hist(f_ebc_3[bot], label="EBCIEM force on free nodes", bins=nbins)
savefig(output_filename * ".plot.histogram-force-dirichlet-boundary-bot.pdf")
# Top
clf()
hist(f_tot_3[top], label="Internal force on free nodes", bins=nbins)
hist(f_ebc_3[top], label="EBCIEM force on free nodes", bins=nbins)
savefig(output_filename * ".plot.histogram-force-dirichlet-boundary-top.pdf")

fig, ax = subplots(2, 3)
ax[1].hist(f_tot_r[top], label="\$f_r^{int}\$ top", bins=nbins)
ax[2].hist(f_tot_3[top], label="\$f_z^{int}\$ top", bins=nbins)
ax[3].hist(f_tot_m[top], label="\$||f_z^{int}||\$ top", bins=nbins)
ax[4].hist(f_tot_r[bot], label="\$f_r^{int}\$ bottom", bins=nbins)
ax[5].hist(f_tot_3[bot], label="\$f_z^{int}\$ bottom", bins=nbins)
ax[6].hist(f_tot_m[bot], label="\$||f_z^{int}||\$ bottom", bins=nbins)
for a in ax
    a.legend()
end
savefig(output_filename * ".plot.histogram-force.pdf")

# ------------------------------------------------------------------------------
# Statistics

using SofTiSim: nrmse

nrmse.(f_tot_3[bot], f_tot_3[top])

using LinearAlgebra

# Displacement at bottom
maximum(f_tot_3[top])
minimum(f_tot_3[top])
norm(f_tot_3[bot] + f_tot_3[top], 0)
norm(f_tot_3[bot] + f_tot_3[top], 1)
norm(f_tot_3[bot] + f_tot_3[top], 2)
norm(f_tot_3[bot] + f_tot_3[top], Inf)

# f_tot on free surface
maximum(f_tot_3[free])
minimum(f_tot_3[free])
norm(f_tot_3[free], 0)
norm(f_tot_3[free], 1)
norm(f_tot_3[free], 2)
norm(f_tot_3[free], Inf)
