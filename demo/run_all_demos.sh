set -e   # exit on all errors

cd cube_compression
julia --color=yes --check-bounds=no -O3 cube_compression.jl --overwrite=true
cd ..

cd Cube_EBCIEM
julia --color=yes --check-bounds=no -O3 Cube_EBCIEM_neohookean.jl --overwrite=true
julia --color=yes --check-bounds=no -O3 Cube_EBCIEM_ogden.jl --overwrite=true
cd ..

cd Cube_EBCIEM_15x15x15
julia --color=yes --check-bounds=no -O3 Cube_EBCIEM_15x15x15.jl --overwrite=true
cd ..

# This is too slow - run this one in parallel instead
#cd cylinder_indentation
#julia --color=yes --check-bounds=no -O3 cylinder_indentation.jl --overwrite=true
#cd ..

cd cube_extension
julia --color=yes --check-bounds=no -O3 cube_extension.jl --overwrite=true
cd ..

cd sheep_brain_extension
julia --color=yes --check-bounds=no -O3 sheep_brain_extension.jl --overwrite=true
cd ..

cd swelling_cylinder
julia --color=yes --check-bounds=no -O3 swelling_cylinder_neohookean_535.jl --overwrite=true
julia --color=yes --check-bounds=no -O3 swelling_cylinder_neohookean_535_ebciem.jl --overwrite=true
julia --color=yes --check-bounds=no -O3 swelling_cylinder_neohookean_6710.jl --overwrite=true
julia --color=yes --check-bounds=no -O3 swelling_cylinder_neohookean_6710_ebciem.jl --overwrite=true
cd ..
