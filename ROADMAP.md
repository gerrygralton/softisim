Goals
-----

- Easy to use (for both users and developers)
    - Easy to read
    - Easy to learn
- Fast
  - Parallel
  - Fast implicit solvers from external packages
  - Fast explicit time stepping with parallel assembly (e.g. CUDA)
- Generic
    - Dimension independent
    - FD, FVM, FEM, meshfree, collocation
    - Non-conforming integration mesh
    - Combine different models with
      independent dimensions (shells in 3D, multiscale 2D within 3D model),
      approximation methods (FD, FVM, FEM and meshfree) and
      solution procedures (implicit, explicit)

Short Term
----------

- Element-free Galerkin method (EFG)
- Finite element method (FEM)
- Explicit solver

Long Term
---------

- Implicit solver
- Finite difference methods
- Finite volume methods
- Interface with fast solvers (e.g. PETSc)
- CUDA
